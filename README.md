# Nos Futurs


## Lancer pelican

### Installation

```
virtualenv env
source env/bin/activate
pip install -r requirements.txt
```
### lancer le serveur
`pelican --autoreload --listen content -s pelicanconf.py -t theme/`

## Liste des templates

* base (menu + about nos futurs)
	* (page) index -> index.html
		* (inc) home-item.html data-type[documentaire,court-metrage,podcast,lexique]
	* (page) projet
		* (inc) lexique.html
	* (page) list-lexique

## champs markdown

* Shift
	* title: shift
	* category: le travail qui vient
	* type: documentaire

* Travail (lexique) 
	title: le travail 
	video

## liste type de box

### type : Lexique
lecteur: youtube
```
Title: le mot  
Slug: le-mot 
Type: lexique
Category: la-saison 
Date: 2010-12-03 10:20
Video: qHnAjBrHFr0
Modified: 2010-12-05 19:30
... info -> texte + images ...
```

### type : Podcast
lecteur: soundcloud 
```
Title: nom du podcast 
Slug: nom-du-podcast
Type: podcast
Category: la-saison 
Date: 2010-12-03 10:20
Audio:
	- id soundcloud
	- id soundcloud

... info -> texte + images ...
```

### type : court-metrage 
lecteur: vimeo 
```
Title: nom du CM 
Slug: nom-du-CM 
Type: court-metrage
Category: la-saison 
Date: 2010-12-03 10:20
Video: id vimeo

... info -> texte + images ...
```

### type : documentaire primaire 
lecteur: auvio 
```
Title: nom du docu 
Slug: nom-du-docu
Type: documentaire-primaire
Category: la-saison 
Tags: uberisation, algorithme, ranking, economie de plateforme
Date: 2010-12-03 10:20
Video: id auvio 

... info -> texte + images ...
```

### À voir

peut-être avoir un nouveau type -> video-debat


### 

Ordre priorité:

- SHIFT
- CURICUL


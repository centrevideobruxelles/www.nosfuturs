<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style/style.css">
    <title>nosfuturs</title>
  </head>
  <body>


    <div class="logo">
      <img src="content/img/documentaire-plateforme-travail-uberisation-nosfuturs.svg" alt="">
    </div>

    <section>
    	<?php
    	   include "libs/Parsedown.php";
    	   $Parsedown = new Parsedown();
    	 ?>
    	
    	 <?php
    	   $text = file_get_contents("content/txt-cvb.txt");
    	   echo $Parsedown->text($text);
    	  ?>
    	
    	<p class="horologe">
    		<img src="content/img/documentaire-plateforme-travail-uberisation-nosfuturs-2.gif" alt="">
    	</p>
			<h2><span>Documenter demain</span></h2>
    	  <?php
    	    $text = file_get_contents("content/txt-nosfutures.txt");
    	    echo $Parsedown->text($text);
    	   ?>
    </section>

		<footer>
			<ul>
				<li><a target="_blank" href="https://www.facebook.com/nosfutursnet">facebook</a></li>
				<li><a target="_blank" href="https://twitter.com/nosfuturs_net">twitter</a></li>
				<li><a target="_blank" href="https://www.instagram.com/nosfutursnet/">instagram</a></li>
				<li><a target="_blank" href="https://www.linkedin.com/company/centre-vidéo-de-bruxelles/">linkedin</a></li>
				<li><a target="_blank" href=" https://my.sendinblue.com/users/subscribe/js_id/2lneg/id/1">newsletter</a></li>

			</ul>

		</footer>
  </body>
</html>

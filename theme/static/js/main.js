var BODY
var BOXCLICK, BOXTEXT, BOX
var BOXTOP, BOXBOTTOM, BOXRIGHT, BOXLEFT
var P_PODCAST
var B_CLOSE
var B_INFO
var _LIST_
var FLEXMAIN
var LETRAVAIL
var EDITO_NF
var ITEMBOX
var BOXMOT
var B_FULL
var BTN_NEXT
var BTN_PREV

function boxmenu(elem) {
	var flextout = document.querySelector('.flexdetout')

	if(BODY.classList.contains("mobile") && flextout.classList.contains('open_box')) {
			close_media(BOXLEFT)
			close_media(BOXRIGHT)
			close_media(BOXTOP)
			close_media(BOXBOTTOM)
			close_media(INTRO_NF)
			close_media(LETRAVAIL)
	}else{
    var etat = document.getElementById(elem)
		if(etat.classList.contains('open')) {
			etat.classList.remove('open')
		} else {
			etat.classList.add('open')
		}
		EDITO_NF.classList.remove('intro')
	}
}

function liens_activation(el){
	if (el.hasAttribute('data-liens')){
		var activation = function(id){
			var lien = document.getElementById(id)
			if ( lien ){
				lien.classList.add('lien-actif')
			}
		}

		var deactivation = function(id){
			var lien = document.getElementById(id)
			lien.classList.remove('lien-actif')
		}

		var id_liens = el.getAttribute('data-liens').split(' ')
		id_liens.forEach(id => activation(id));

	}
}

function resizeIframe(){
	var iframe = document.getElementById('iframe_documentaire')
	var iw = 640
	var ih = 360
	if (BODY.classList.contains('mobile')){
		var btop = window.innerHeight
		var bwid = window.innerWidth
		var wid = (iw / bwid) * iw
		iframe.width = wid
	} else {
		var btop = window.innerHeight / 2
		var bwid = window.innerWidth / 2.2
		if (iw > bwid) {
			var scale = bwid / iw
		} else {
			var scale = btop / ih
		}
		iframe.style.transform = 'scale('+scale+')'
	}
}

function findPos(obj) {
	var curtop = 0;
	if (obj.offsetParent) {
		do {
			curtop += obj.offsetTop;
		} while (obj = obj.offsetParent);
		return [curtop];
	}
}

function scrollSubtitle(el) {
	el.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
}

function playIndexLexique(el){
	var box = document.querySelector("#box-right > .content")
	console.log(box)
	var p = el.getAttribute('data-position')
	var BOXMOT = document.querySelector('.box-mot')
	var iframes = BOXMOT.querySelectorAll('iframe')
	setTimeout(function(){
		var ele = iframes[parseInt(p) - 1]
		ele.src = ele.src + '?autoplay=1'
		ele.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
	}, 1000);
	// ele.scrollIntoView();
	// box.scroll(0,findPos(ele))
}

function deleteAudio(){
	console.log(audio)
	if (audio) {
		audio.pause()
		audio.src = ""
		delete audio
	}
}

function swith_episode(type){
	var id = document.querySelector('#box-bottom .player').getAttribute('data-id')
	if (type == "next") {
		var el = document.querySelector('#'+id).nextSibling.nextElementSibling

	} else {
		var el = document.querySelector('#'+id).previousSibling.previousElementSibling
		console.log(el)
	}
	if (el !== null) {
		deleteAudio()
	}

	console.log(el)
	xh(el)
}

function xh(el) {
	var url = el.getAttribute('data-url')
	var playerId = el.getAttribute('data-player')
	xhr = new XMLHttpRequest()
	xhr.open("GET", url, false)
	xhr.onreadystatechange = function() {
		if (xhr.readyState === 4 && xhr.status == "200") {
			var c = xhr.responseText
			var P_ = document.getElementById('box-'+playerId)
			P_.querySelector('.content').innerHTML = c

			if(playerId == 'left'){loadNav(P_)}
			
			if(playerId == 'bottom') {

				BTN_NEXT = document.querySelector('.next')
				BTN_PREV = document.querySelector('.prev')

				BTN_NEXT.addEventListener('click', function(){
					swith_episode("next")
				})
				BTN_PREV.addEventListener('click', function(){
					swith_episode("previous")
				})
				btn_info()
				deleteAudio()
				player()
			}
			if (el.classList.contains('mot-lexique-selection')) {
				playIndexLexique(el)
			}
			if (el.classList.contains('documentaire-primaire')){
				resizeIframe(el.offsetHeight)
			}
			liens_activation(el)
			P_.classList.add('open')
			FLEXMAIN.classList.add('open_'+playerId)
			FLEXMAIN.classList.add('open_box')
			setTimeout(function(){
				placement()
			}, 1000);
		}
	}
	xhr.send("")
}

function loadNav(P_) {
	console.log(P_)
	var subTitles = P_.querySelectorAll('H4')
	var header = P_.querySelector('.header') 
	var OL = document.createElement('OL')
	OL.id = 'nav_left'
	if (subTitles.length > 0) {
		subTitles.forEach(function(item, i){
			item.id = "subtitle_"+i
			OL.innerHTML += '<li class="nav_item" data-id="subtitle_'+i+'" >'+item.innerHTML+'</li>'
		})
		header.innerHTML += OL.outerHTML
		var LIS = document.querySelectorAll('.nav_item')
		console.log('------> ', LIS)

		LIS.forEach(function(eleme, i){
			console.log('------> ', eleme)
			eleme.addEventListener("click", function(){
				var subT = document.getElementById(this.getAttribute('data-id'))
				subT.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
			});
		})
	}
}

function click_media() {
	BOXCLICK.forEach(el => load_media(el));
}

function load_media(el) {
	el.addEventListener('click', function(){
		composeHash(this)
		xh(el)
	})
}

function close_media(el){
	console.log(el)
	if (el.classList.contains('open')){
		el.classList.remove('open')
		pos = el.id.split('-')[1]
		FLEXMAIN.classList.remove('open_'+pos)
		FLEXMAIN.classList.remove('open_box')
		el.classList.remove('box-fullscreen')
		document.querySelector('.reader-'+pos+' .content').innerHTML = ""
		setTimeout(function(){
			placement()
		}, 1000);
		removeHash(el.id.split('-')[1].charAt(0))
	}
}

function btn_close_media() {
	B_CLOSE.forEach(function(item, i){
		item.addEventListener('click', function(){
			var par = this.closest('.box-reader')
			close_media(par)
		})

	})
}

function btn_info(){
	var btns = document.querySelectorAll('.btn_info')
	btns.forEach(function(item, i){
		item.addEventListener('click', function(){
			BOXBOTTOM.classList.toggle('box-fullscreen')
		})
	})
}

function adjustForm(){
	BOX.forEach(function(item,i){
		var w = item.parentNode.clientWidth
		var h = item.parentNode.clientHeight
		item.style.width= w + 'px'
	})
}

function btn_fullscreen(){
	B_FULLSCREEN.forEach(function(item, i){
		item.addEventListener('click', function(){
			var par = this.closest('.box-reader')
			par.classList.toggle('box-fullscreen')
		})
	})
}

function removeElement(array, elem) {
    var index = array.indexOf(elem);
    if (index > -1) {
        array.splice(index, 1);
    }
}

function splitHash(){
	if (location.hash){
		ids = location.hash.split('#')[1].split('__')
		removeElement(ids, "")
		return ids
	}else{
		return []
	}
}

function composeHash(el){
	var newId = el.id
	var pos = el.getAttribute('data-player').charAt(0)
	var ids = splitHash()
	var newHash = []
	ids.forEach(function(id){
		if (id.split('::')[0] !== pos){
			newHash.push(id)
		}
	})
	newHash.push(pos+'::'+newId)
	location.hash = newHash.join('__')
}

function removeHash(pos){
	var newHash = []
	splitHash().forEach(function(id){
		if (pos !== id.split('::')[0] ){
			newHash.push(id)
		}
	})
	location.hash = newHash.join('__')
}


function activeHash(){
	EDITO_NF.classList.remove('intro')
	splitHash().forEach(function(id){
		BOXCLICK.forEach(function(item){
			if (item.id == id.split('::')[1] ){
				INTRO_NF.classList.remove('open')

				xh(item)
				return
			}
		})
	})
}

function returnIndex(){
	window.location.href = 'index.html'
}

window.addEventListener('DOMContentLoaded', function(){
	BODY = document.querySelector('BODY')
	BOXTOP = document.querySelector('#box-top')
	BOXBOTTOM = document.querySelector('#box-bottom')
	BOXRIGHT = document.querySelector('#box-right')
	BOXLEFT = document.querySelector('#box-left')
	BOXCLICK = document.querySelectorAll('.box-click')
	BOXTEXT = document.querySelectorAll('.box-Lexique-Text')
	P_PODCAST = document.getElementById('box-playerPodcast')
	B_CLOSE = document.querySelectorAll('.close')
	B_FULLSCREEN = document.querySelectorAll('.fullscreen')
	FLEXMAIN = document.querySelector('.flexdetout')
	LETRAVAIL = document.querySelectorAll('.category,#le-travail-qui-vient')
	INTRO_NF =  document.querySelector('#box-edito')
	EDITO_NF =  document.querySelector('.reader-edito.intro')
	BOX = document.querySelectorAll('.forme')
	ITEMBOX = document.querySelector('main')
	ITEM = document.querySelector('.item')
	BTN_NEXT = document.querySelector('.next')
	BTN_PREV = document.querySelector('.prev')

	activeHash()
	btn_close_media()
	document.querySelector('#removeBox').addEventListener('click', function(){
		close_media(BOXLEFT)
		close_media(BOXRIGHT)
		close_media(BOXTOP)
		close_media(BOXBOTTOM)
		deleteAudio()
	})

	click_media()
	adjustForm()
	btn_fullscreen()
	mobile()

	const resize_ob = new ResizeObserver(function(entries) {
		let rect = entries[0].contentRect
		let width = rect.width
		let height = rect.height

		adjustForm()

		if(FLEXMAIN.classList.contains('open_top')){
			resizeIframe()
		}

	})

	resize_ob.observe(ITEMBOX);

	LETRAVAIL.forEach(function(item, i){
		item.addEventListener("click", function(){
			LETRAVAIL[1].classList.toggle('open')
			placement()
		})
	})

	if (location.hash == '#home') {
		LETRAVAIL.removeAttribute("open")
	}

	EDITO_NF.addEventListener("click", function(){
		EDITO_NF.classList.remove('open')
		console.log(intro)
		if(EDITO_NF.classList.contains('intro')){
			EDITO_NF.classList.remove('intro')
		}
		var intro = EDITO_NF.querySelector('.EditoNav')
		if(intro.classList.contains('intro')){
			intro.classList.remove('intro')
		}
		placement()
	}, {once:true})

	var closeEDITO_NF = EDITO_NF.querySelector('.close')

	window.addEventListener("hashchange", function(){
		var hash = window.location.hash.substring(1)
		if(hash == '') {
			close_media(BOXLEFT)
			close_media(BOXRIGHT)
			close_media(BOXTOP)
			close_media(BOXBOTTOM)
			close_media(INTRO_NF)
			close_media(LETRAVAIL)
		}
	});

	placement()
})

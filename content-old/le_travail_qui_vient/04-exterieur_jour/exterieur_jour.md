Title: Extérieur Jour-1
slug: exterieur-jour-1
Category: le-travail-qui-vient
Type: court-metrage
Template: article 
Niveau: 3
Date: 2010-12-03 10:20
video:
    - <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://videos.domainepublic.net/videos/embed/3df01fc5-78f5-4ed8-8406-2fec2367ea88?title=0&warningTitle=0&peertubeLink=0" frameborder="0" allowfullscreen></iframe>

## Un court métrage collectif

### Le réseaux P2P et son application à l'économie collaborative

Face au développement des plateformes, certains cherchent à mettre en place des **alternatives**. Rencontre avec la P2P Fondation.

*Ce film est réalisé au sein du collectif Extérieur Jour: un collectif permanent et ouvert à tou·te·s les bruxellois·e·s de 18 à 90 ans.*

![](img/01.jpg)
![](img/02.jpg)
![](img/03.jpg)

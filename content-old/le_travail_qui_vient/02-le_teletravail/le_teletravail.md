Title: Le télétravail
slug: le-teletravail
Category: le-travail-qui-vient
Type: documentaire
Niveau: 2
Date: 2010-12-03 10:20
Video:
    3oiLx7Sf-X4


###Le télétravail a été poussé sur le devant de la scène pendant le confinement du printemps 2020. En soi, et selon la plupart des syndicats, il n'est ,i bien ni mal. Il lui faut en revanche des balises claires pour **empêcher les dérives** (isolement des travailleurs, manque de formation, surveillance accrue, risque de travail continu...).

####*Les "films-outils" développés par le CVB sont pensés comme des outils au service des associations et des citoyens.*

![](img/01.jpg)

Title: Curriculum Futuræ
slug: curriculum-futuræ
Category: le-travail-qui-vient
Type: podcast
Niveau: 2
Date: 2010-12-03 10:20
Video: <iframe width="100%" height="330" scrolling="no" frameborder="no" src="https://audio.liquorstane.ynh.fr/front/embed.html?&amp;type=album&amp;id=18"></iframe>

## Un podcast en 5 épisode de Sébastien Dicenaire

### Un père de famille anticipe (beaucoup) la recherche de travail de ses enfants

À l'heure de l'ubérisation, de la robotisation des tâches et des algorithmes auto-apprenants, un père trentenaire un brin flippé s'inquiète de l'avenir professionnel de ses jumeaux de 5 ans... À la rencontre de spécialistes de tous bord comme de travailleur·euse·s actuel·le·s, il mène l'enquête sur les **scénarios futurs** du travail.

*Sébastien Dicenaire est écrivain, performeur et créateur radiophonique ...*

![](img/01.jpg)

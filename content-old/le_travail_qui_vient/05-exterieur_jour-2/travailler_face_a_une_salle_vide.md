Title: Extérieur Jour-2
slug: exterieur-jour-2
Category: le-travail-qui-vient
Type: court-metrage
Template: article 
Niveau: 3
Date: 2010-12-03 10:20
Video: <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://videos.domainepublic.net/videos/embed/3df01fc5-78f5-4ed8-8406-2fec2367ea88?title=0&warningTitle=0&peertubeLink=0" frameborder="0" allowfullscreen></iframe>

## Un court métrage collectif
### L'avenir du travail artistique dans un monde de plus en plus virtuel
La crise sanitaire a jeté dans le trouble tout le secteur culturel, quel sens pour le travail des artistes face à des **salles vides**? Les métiers de l'ombre de la culture se sont structurés sous formes de syndicat pendant le confinement.

![](images/01.jpg)
![](images/02.png)

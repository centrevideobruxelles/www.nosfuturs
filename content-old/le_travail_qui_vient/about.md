Title: Le travail qui vient
Slug: le-travail-qui-vient
Template: item_projet
Date: 2010-12-03 10:20
Modified: 2010-12-05 19:30
Category: theme
Summary: Short version for index and feeds
Tags: uberisation, algorithme, ranking, economie de plateforme, flexijob, data, teletravail
Status: published

Dans la sphère du travail, la novlangue numérique a fait son apparition depuis plusieurs année maintenant: algorithme, plateforme, économie collaborative, flexibilité, intelligence artificielle, ranking, télétravail s'immiscent dans le quotidien des travailleurs.

La numérisation bouscule l'emploi et le rapport au travail de manière inédite.

Pour inaugurer nosfuturs.net, le [CVB](https://cvb.be/) vous propose de plonger au cœur des nouvelles formes de travail. Qu'advient-il des humains derrière les plateformes? Peut-on parler — comme le font les dirigeants de "l'économie collaborative"  - de liberté lorsqu'il s'agit de précarité, d'indépendance lorsqu'il s'agit de flexibilité? Quelles conséquences la généralisation du télétravail aura-t-elle sur les travailleurs et leur famille? Comment le tout numérique transforme-t-il le rapport au travail? Quel futur pour les artistes dans un monde de plus en plus virtuel?

Si aujourd'hui l'algorithme tend à remplacer le contremaître, que les chaînes ne sont plus celles des usines, mais celles de vélos de coursier au cœur de nos villes; la cadence, elle, reste la même. Et les travailleurs s'isolent.  Comment s'organiser dès lors pour conserver les acquis sociaux obtenus de longues luttes?

Le [Centre Vidéo de Bruxelles](https://cvb.be/) entend faire résonner différents points de vue, pour offrir aux citoyens, un outil de réflexion sur le travail qui vient.
Mettons pause dans cette mécanique qui certains jours nous emportes tpoutes et tous, pour lancer le débat: quel travail pour demain

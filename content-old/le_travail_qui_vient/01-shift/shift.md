Title: Shift
Slug: shift
Category: le-travail-qui-vient
Type: documentaire-primaire
Niveau: 1
Date: 2010-12-03 10:20
Liens: uberisation algorithme ranking economie-de-plateforme
Video: <iframe sandbox="allow-same-origin allow-scripts allow-popups" src="https://videos.domainepublic.net/videos/embed/3df01fc5-78f5-4ed8-8406-2fec2367ea88" frameborder="0" allowfullscreen></iframe>



## Un film documentaire de Pauline Beugnies
Test Le parcours d'un ancien livreur attaqué en justice par Deliveroo l'histoire d'un coursier avec plus de 20.000km au compteur qui se bat quotidiennement contre un [algorithme](#algorithme). Partant de **l'histoire singulière** de Jean-Bernard, assigné en justice aux cotés de l'État belge par Deliveroo, le film raconte sa tranformation personnelle et son combat face aux conditions de travail et au projet de société du capitalisme des plateformes. En arrière fond, la remise en question des acquis sociaux et l'évitement du droit du travail.

*Pauline Beugnies est une réalisatrice et photographe belge, elle a récemment reçu le Prix ...*

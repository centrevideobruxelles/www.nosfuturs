Title: À propos
slug: about
Template: about
category: about
Date: 2010-12-03 10:20
Modified: 2010-12-05 19:30
Summary: description home

#nosfuturs.net
Engagé depuis plus de 40 ans dans la production de conteus audiovisuels, le [CVB - Centre vidéo de Bruxelles](https://cvb.be/) élargit en 2021 son champ du cinéma documentaire à celui du transmédia. Le portail <span class="logo">nosfuturs.net<span> rassemble une constellation de projets artistiques et engagés autour d'une même thématique. <span class="logo">nosfuturs.net<span> abolit les frontières formelles pour ne plus penser depuis l'entité d'un monde unique mais en s'ouvrant à la pluralité des mondes, et réfléchir ensembles aux grands enjeux de société de demain.

Imaginée avant la pandémie de Covid-19, <span class="logo">nosfuturs.net<span> prend une toute autre ampleur à l'aune des mois que la planète traverse. tous les regards sont dorénavant tournés vers ce "monde d'après", à la fois source d'espoir et d'inquiétude. En quelques mois nous l'avons tous éprouvé: le futur est déjà là.

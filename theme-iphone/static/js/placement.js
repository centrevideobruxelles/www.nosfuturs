

function show(change) {
   document.getElementById('edito').innerHTML = document.getElementById(change).innerHTML;
 }

function getRandomNumber(min, max) {
	return Math.floor(Math.random() * (max - min) + min);
}

function check_(_LIST_, _new_){
	var repp = true
	_LIST_.forEach(function(item){
		var vv = []
		for (var i = 0; i < _new_.length; ++i) {
			if (item[i] == _new_[i]){
				vv.push(false)
			}else{
				vv.push(true)
			}
		}
		if (vv.includes(false)) return false;
	})
	return true 
}


function placement(){
	var item = document.getElementsByClassName('item')[0]
	var itemHeight = item.offsetHeight
	var itemWidth = item.offsetWidth
	var itemTop = item.offsetTop
	var itemLeft = item.offsetLeft
	var max_Top_ = itemHeight/1.8 + itemTop
	var min_Top_ = itemHeight + itemTop
	var _Left_ = itemWidth + itemLeft

	var boxlexique = document.getElementsByClassName('lexique')
	
	var pos = function(){
		var Top = getRandomNumber(min_Top_, max_Top_ - 80)
		var Left = getRandomNumber(_Left_/1.8, _Left_ - 80)
		var Rot = getRandomNumber(0, 90)
		return [Top, Left, Rot]
	}

	for (var i=0; i < boxlexique.length; i++) {
		var thisDiv = boxlexique[i]
		var u = 1
		var poss = pos()

		thisDiv.style.marginTop = (poss[0] * u) + "px"
		thisDiv.style.marginLeft = (poss[1] + u ) + "px"
		thisDiv.style.transform = "rotate("+poss[2]+'deg)' 
	}

}

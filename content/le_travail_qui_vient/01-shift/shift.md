Title: Shift
SubTitle: Pauline Beugnies | 2021 | BE | VO FR | 61'
Slug: shift
Category: le-travail-qui-vient
Type: documentaire-primaire
Template: documentaire-primaire
Niveau: 1
Date: 2010-12-03 10:20
Liens: lexique_deconnexion lexique_algorithme_6 lexique_algorithme lexique_economie-de-plateforme_4 lexique_ranking_1 lexique_ranking_2 lexique_uberisation_6


<!--<iframe src="https://iframe.dacast.com/vod/affcee6a-fb4e-d06f-e78b-02ff8ab4cd8e/ed34a878-6a94-7bfe-6377-c89e4dfd4cda" width="50%" height="100%" frameborder="0" scrolling="no" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe> -->


<!--video: 2763989 -->
<!-- Liens: deliveroo_nous conference-pointculture-1 curriculum-futurae-1 deconnexion algorithme-6 algorithme economie-de-plateforme-4 ranking_1 ranking-2 uberisation-6 -->

SHIFT, c'est l'histoire d'un coursier avec plus de 20.000 km au compteur, qui a porté le « maillot à pois », qui se bat quotidiennement contre un algorithme. Un jour, ce coursier dit que non, il n'est pas d'accord d'être payé à la tâche malgré la « coolitude » de son métier devenu un « flexi-job ».

Partant de l'histoire singulière de Jean-Bernard Robillard, poursuivi en justice aux côtés de l’État belge par Deliveroo, SHIFT raconte l’histoire d’une transformation personnelle et d’un combat face aux conditions de travail et au projet de société du capitalisme de plateformes.
<br>
-- 
<br>
A one-hour immersion in the life of a bike courier, who rode more than 20.000 km, wore the Polka Dot Jersey and fought against an algorithm. SHIFT tells the story of a personal transformation and a struggle against the working conditions and the social project of platform capitalism.
<br>
<html><details><summary>Plus d'infos</summary></html>
De jour comme de nuit, les coursiers à vélo font désormais partie de notre décor urbain. Mais pour visibles qu’ils soient avec leurs tenues ou sacs réfléchissants, les conditions dans lesquelles ils travaillent restent méconnues, tout autant que le fonctionnement des plateformes numériques qui les embauchent à tour de bras et à bas prix. Ils sont la partie émergée de cette « nouvelle économie » numérique qui s’immisce chaque jour un peu plus dans notre quotidien et avec elle de nouveaux modes d’intermédiation, de production et d’organisation du travail.

Au-delà du parcours singulier de Jean-Bernard, cette histoire met à jour le fonctionnement d’un modèle économique qui, s’il permet au citoyen de consommer autrement, se base sur un business model s’inscrivant dans une logique ultralibérale du « tout au marché » et de dérégulation.

En requalifiant le statut de « faux indépendants » des coursiers à vélo de Deliveroo, Uber Eats, Glovo et Stuart en statut de salariés, la « Loi Riders » récemment votée en Espagne ouvre la voie aux autres tribunaux nationaux européens pour une régularisation du statut des livreurs.

En Belgique, l’enquête pénale de deux ans menée par l’ONSS pour l’auditorat du travail de Bruxelles, a débouché sur une assignation de Deliveroo devant le tribunal du travail. Le début du procès a été fixé au mois d’octobre 2021.
<html></details></html>

<html><details><summary>Pauline Beugnies, la réalisatrice</summary></html>

**Pauline Beugnies** est née à Charleroi en 1982. Elle a étudié le journalisme à l’IHECS à Bruxelles.

Son premier reportage photo était consacré aux enfants des rues de Kinshasa. Pauline a vécu au Caire. Elle a suivi de près la jeunesse, à l’avant-garde des mouvements de révolte populaires. Sa première exposition solo « The Revolution of the Youth » a lieu en 2012 au Brakke Ground à Amsterdam. Elle co-réalise le documentaire web [« Sout el shabab, la voix des jeunes »](https://www.dailymotion.com/video/x13rlmq){:target="_blank"}, en partenariat avec France Culture. Le projet gagne le Mediterranean Journalism Award de la Fondation Anna Lindh. Le Nikon Press Photo Award 2013 lui est attribué pour son travail « Battir, l’intifada verte ». Son premier livre, « Génération Tahrir », sort en janvier 2016 aux éditions du Bec en l’Air. Il est sélectionné pour le Prix du livre photo-texte des rencontres photographiques d'Arles.

Son premier film documentaire [« Rester Vivants »](https://www.rayuelaprod.com/film/rester-vivants/){:target="_blank"}, dans la continuité de son travail photographique, est sélectionné à DokLeipzig, gagne le prix Scam Belgique en 2017 et est nommé aux Magritte du cinéma. En 2018, [Derrière le Soleil](https://www.bps22.be/fr/expositions/pauline-beugnies){:target="_blank"}, travail qui intègre photos, vidéos, et archives, a été exposé au BPS22. Son premier court métrage de fiction, [« Shams »](https://www.agenceducourtmetrage.be){:target="_blank"}, une histoire d'amour au Caire, est sorti en 2020 et a gagné deux prix au Brussels Short Film Festival.« Shift » est son deuxième documentaire.
<html></details></html>

- **Écriture** : Pauline Beugnies, avec la collaboration de Jean-Bernard Robillard
- **Réalisation** : Pauline Beugnies
- **Image** : Loïc Carrera
- **Son** : Edith Herregods
- **Montage** : Salvatore Fronio  
- **Montage son** : Julien Mizac  
- **Animation** : Jean Forest
- **Mixage** : Jean-François Levillain
- **Étalonnage** : Maxime Tellier
- **Musique originale** : Loup Mormont
- **Production déléguée** : Cyril Bibas
- **Direction de production** : Marc Jottard & Joel Curtz
- *SHIFT* est une production du [CVB - Centre Vidéo de Bruxelles](https://cvb.be/fr/films/shift){:target="_blank"} en coproduction avec la RTBF - Unité Documentaire, Proximus & Thowra asbl avec l’aide du Centre du Cinéma et de l’audiovisuel & le soutien du Fonds pour le journalisme de la Fédération Wallonie-Bruxelles.

Title: À scène ouverte
SubTitle: Collectif Extérieur Jour | 2021 | BE | VO FR | 15'
slug: a-scene-ouverte
Category: le-travail-qui-vient
Type: court-metrage
Template: court-metrage 
Niveau: 3-3
Date: 2010-12-03 10:20
Liens: curriculum-futurae lexique_algorithme_8 lexique_algorithme_5 lexique_uberisation_5 lexique_ranking_3 conference-pointculture
Video: ylnokTxJ27w
cover: 04-exterieur_jour/03.jpg

### Comment vivre de sa musique maintenant que les concerts sont online ?

L’une des réponses au bouleversement engendré, à la fin des années 90, par l’apparition du format MP3, fut de miser sur la musique live. Avec l’arrêt brutal des concerts dû à la pandémie de covid, c’est une autre pratique de diffusion et de consommation de la musique qui est aujourd’hui en plein essor : le live stream.
Si le live stream profite bien évidemment aux plateformes en ligne, qu’en est-il de toute la chaîne des métiers de la scène musicale, à commencer par les auteurs, compositeurs et interprètes eux-mêmes ? Et, au-delà, cette accélération de la numérisation de la musique ne risque-t-elle pas d’uniformiser les pratiques des professionnelles comme du public ?

« A scène ouverte » a été réalisé par le Collectif Extérieur Jour. La présente version de 15’ est une version raccourcie du format 26’ produit par le [CVB](https://cvb.be/fr/films/a-scene-ouverte){:target="_blank"} et diffusé sur BX1.

<html><details><summary>Le collectif Extérieur Jour</summary>
Extérieur Jour est un collectif permanent ouvert à tous les bruxellois de 18 à 90 ans. Son objet : créer un espace de parole audiovisuel - un film de 26 minutes à plusieurs - pour mieux comprendre le monde dans lequel nous vivons. Toutes les 4 semaines, une dizaine de personnes, accompagnées de deux animateurs, franchissent les étapes du choix du sujet et du point de vue, de l’organisation logistique, de l’interview et des cadrages, de la prise de son jusqu’au montage et à la sonorisation. Grâce à un partenariat avec BX1 Médias de Bruxelles, les films sont diffusés deux dimanches par mois à 14h sur la télévision régionale bruxelloise.
</details></html>

- **Réalisé par** : Alma, Aline, Diego, Elias, Gabrielle, Roman
- **Avec la participation de** : Florent Le Duc, Alice Vande Voorde, Marie Warnant, Didier Gosset
- **Musique** : Lupen Block – Plagasul, A land of misteries – PCRUZN
- **Coordination et Animation** : Olivier CHARLIER et Anne-Lyse SOUQUET
- **Montage** : Salvatore FRONIO
- **Animation After Effects** : Diego Rodrigez
- **Productrice déléguée** : Louise LABIB
- *Musique additionnelle* : LES SECRÈTES SESSIONS #15 : Alice Vande Voorde, Antoine Loyer, Cédric Van Caillie, Ciska Thomas, Diane De Moor, Florent Leduc, Leslie Gutierrez, Lucie Lefauconnier, Maria Green, Nicolas Allard, Pierre René Nicolas, Pierre Leroy, Renaud Ledru, Roselien Tobbackx, Thècle Joussaud

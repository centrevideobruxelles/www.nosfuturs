Title: La peer enquête
SubTitle: Collectif Extérieur Jour | 2021 | BE | VO FR | 22'
slug: la-peer-enquete
Category: le-travail-qui-vient
Type: court-metrage-2
Template: court-metrage
Niveau: 3-3
Date: 2010-12-03 10:20
Liens: a-distance
Video: hmwMPgICJmk
cover: 04-exterieur_jour/03.jpg


### Comment collaborer librement autour de ressources mutualisées ?

Vous ne le savez peut-être pas quand vous commandez une pizza via une application telle qu’Uber Eats ou Deliveroo, mais généralement, votre livreur n’est ni salarié, ni indépendant. Ces deux plateformes travaillent via un régime fiscal a priori avantageux : le statut P2P. Qu'on appelle aussi le  "pair-à-pair". Nous avons enquêté sur les origines de ce statut. Quelle différence entre un P2P vertueux et un système qui le détourne pour mieux exploiter ses travailleurs? #LoiDeCroo

Michel Bauwens, nous explique que le pair à pair, à l'origine, vient d'un concept informatique. Chaque ordinateur peut se connecter et échanger directement en se passant d'un serveur. Les gens peuvent se connecter entres-eux et s'auto-organiser localement ou globalement sans intermédiaire.
De plus en plus de projets orientés "commun" se mettent en place partout dans le monde. A Bruxelles, nous avons rencontré et passé sous la loupe le fonctionnement du collectif Communa et de la Micro-Factory. 



« La peer enquête » a été réalisé par le Collectif Extérieur Jour.


<html><details><summary>Le collectif Extérieur Jour</summary>
Extérieur Jour est un collectif permanent ouvert à tou.te.s les bruxellois.es de 18 à 90 ans. Son objet : créer un espace de parole audiovisuel - un film de 26 minutes à plusieurs - pour mieux comprendre le monde dans lequel nous vivons. Toute les 4 semaines, une dizaine de personnes, accompagnées de deux animateurs, franchissent les étapes du choix du sujet et du point de vue, de l’organisation logistique, de l’interview et des cadrages, de la prise de son jusqu’au montage et à la sonorisation. Grâce à un partenariat avec BX1 Médias de Bruxelles, les films sont diffusés deux dimanches par mois à 14h sur la télévision régionale bruxelloise.
</details></html>

- **Réalisé par** : Boris, Celia, Joseph, Thomas,Tim
- **Avec la participation de** : Michel Bauwens, Margaud Antoine-Fabry, Antoine Dutrieu, Gilles Pinault, Veronique Van Espen
- **Coordination et Animation** : Olivier CHARLIER et Anne-Lyse SOUQUET
- **Montage & habillages visuels** : Lucas FURTADO
- **Productrice déléguée** : Louise LABIB
- **Musique** :
Noir#1 : Music by Pedro, promoted by Mr. Snooze
Night on the docks - Covert affair &  Dances and dames by Kevin Macleod, promoted by Mr. Snooze
- **Production** : CVB

Title: Télétravail et combat syndical
SubTitle:
slug: teletravail-et-combat-syndical
Category: a-distance
Type: court-metrage
Template: outil-pedagogique
Niveau: 2
Date: 2010-12-03 10:20
Video: Hk4gbdHJtnc

Qu’en est-il du travail syndical dès lors que les travailleurs sont chez eux, atomisés ? Quel avenir pour le combat collectif ? Sans nul doute le télétravail obligera les syndicats à inventer de nouvelles  formes de mobilisation et de lutte collectives.

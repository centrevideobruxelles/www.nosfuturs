Title: Le télétravail, écologique ?
SubTitle:
slug: le-teletravail-ecologique
Category: a-distance
Type: court-metrage
Template: court-metrage
Niveau: 2
Date: 2010-12-03 10:20
Video: afiW5sjEDIw

Vendu comme une solution au réchauffement climatique, le télétravail a vite fait de se parer des vertus d’un dispositif décarboné. Vraiment ? Des déplacements qui se déplacent ailleurs aux bureaux qui ne ferment jamais jusqu’à l’augmentation considérable des besoins en énergie, le télétravail serait bien moins écologique qu’on ne le pense.

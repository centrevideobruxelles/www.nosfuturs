Title: Télétravail et non-marchand
SubTitle:
slug: teletravail-et-non-marchand
Category: a-distance
Type: outil-pedagogique
Template: outil-pedagogique
Niveau: 2
Date: 2010-12-03 10:20
Video: 6CTAod7yTHc

Le télétravail est une des nouvelles modalités de l’organisation du travail. Mais qu’en est-il dans le non-marchand, ce secteur où le lien et la présence physique sont au cœur des processus de travail ? Ici aussi, selon toute vraisemblance, le travail à distance occupera davantage de place à l’avenir. Pour un mieux ?

<iframe class="capsules" width="40%" rel="0" height="20%" src="https://www.youtube.com/embed/afiW5sjEDIw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
####**Télétravail, écologique ?** <br>avec Maxime Bruggeman de la cellule mobilité de la CSC, Fanny Lederlin -  Doctorante en philosophie

<iframe class="capsules" width="40%" rel="0"  height="20%" src="https://www.youtube.com/embed/6CTAod7yTHc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
####**Télétravail et non-marchand ?** <br>avec Véronique Van Espen - Coordinatrice de l'ABBET (association bruxelloise pour le bien-être au travail), Amik Lemaire - Directeur du Centre culturel de Jette

<iframe class="capsules" width="40%" rel="0"  height="20%" src="https://www.youtube.com/embed/xbkhem0Lq9A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
####**Télétravail et management ?** <br>avec Alain Piette du SPF Emploi, Véronique Van Espen, Amik Lemaire

<iframe class="capsules" width="40%" rel="0"  height="20%" src="https://www.youtube.com/embed/irY6noOWDP0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
####**Télétravail et syndicalisme ?** <br>avec Marc Gillain- délégué syndical, Manon Van Thorre - service entreprises de la CSC, Sandrino Graceffa - Chercheur en Sciences sociales

*Ces capsules vidéo viennent compléter le film sur le télétravail : "à distance"*

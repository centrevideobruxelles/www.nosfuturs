Title: Le télétravail-capsules
SubTitle:
slug: le-teletravail-capsules
Category: le-travail-qui-vient_off
Type: court-metrage
Niveau: 3
Date: 2010-12-03 10:20
video:
- <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://videos.domainepublic.net/videos/embed/3df01fc5-78f5-4ed8-8406-2fec2367ea88?title=0&warningTitle=0&peertubeLink=0" frameborder="0" allowfullscreen></iframe>
Télétravail, écologique ? avec Maxime Bruggeman de la cellule mobilité de la CSC, Fanny Lederlin -  Doctorante en philosophie

- <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://videos.domainepublic.net/videos/embed/3df01fc5-78f5-4ed8-8406-2fec2367ea88?title=0&warningTitle=0&peertubeLink=0" frameborder="0" allowfullscreen></iframe>
Télétravail et non-marchand ? avec Véronique Van Espen - Coordinatrice de l'ABBET (association bruxelloise pour le bien-être au travail), Amik Lemaire - Directeur du Centre culturel de Jette

- <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://videos.domainepublic.net/videos/embed/3df01fc5-78f5-4ed8-8406-2fec2367ea88?title=0&warningTitle=0&peertubeLink=0" frameborder="0" allowfullscreen></iframe>
Télétravail et management ? avec Alain Piette du SPF Emploi, Véronique Van Espen, Amik Lemaire

- <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://videos.domainepublic.net/videos/embed/3df01fc5-78f5-4ed8-8406-2fec2367ea88?title=0&warningTitle=0&peertubeLink=0" frameborder="0" allowfullscreen></iframe>
Télétravail et syndicalisme ? avec Marc Gillain - délégué syndical, Manon Van Thorre - service entreprises de la CSC, Sandrino Graceffa - Chercheur en Sciences sociales

Ces capsules video viennent complété le film sur le télétravail : "à distance"
*Les "films-outils" développés par le CVB sont pensés comme des outils au service des associations et des citoyens.*

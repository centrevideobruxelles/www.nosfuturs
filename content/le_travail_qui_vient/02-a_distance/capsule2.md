Title: Télétravail et management
SubTitle:
slug: teletravail-et-management
Category: a-distance
Type: court-metrage
Template: court-metrage
Niveau: 2
Date: 2010-12-03 10:20
Video: xbkhem0Lq9A

Vous l’avez sans doute déjà expérimenté : votre n+1 ne sait plus trop de quoi est fait son métier face au télétravail généralisé. Contrôle excessif versus autonomie incontrôlée ? Le télétravail vient bousculer et remettre en perspective les pratiques du management tel qu’enseigné dans les écoles.

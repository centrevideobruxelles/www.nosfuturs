Title: À distance
SubTitle: À DISTANCE | Michel Steyaert | 2021 | 40’ + 4 x 10’
slug: a-distance
Category: le-travail-qui-vient
Type: outil-pedagogique
Template: outil-pedagogique
Niveau: 3-1
Date: 2010-12-03 10:20
Liens: alter-echo-article conference-pointculture-enseignant le-teletravail-ecologique teletravail-et-management teletravail-et-non-marchand teletravail-et-combat-syndical lexique_teletravail_6 lexique_teletravail_5 lexique_deconnexion_2 lexique_travail_4 lexique_teletravail_4
Video: mr-uWwsPUco

Jusqu’en mars 2020, le télétravail ne concernait qu’un très faible pourcentage de notre population active. Souvent considéré comme une faveur, fantasmé par une partie des salariés, avec la crise du covid, le travail à domicile ou télétravail se prolonge et se généralise.

Que faut-il en attendre pour les travailleurs? En sortiront-ils gagnants? Disparition du collectif, isolement, augmentation du temps de travail, invasion de la sphère privée, etc. Quand une majorité de belges dit vouloir désormais télétravailler davantage, "À distance" part à la rencontre d'expériences concrètes, de vécus singuliers, pour amorcer la réflexion en compagnie de philosophes, sociologues, syndicalistes et managers.



<br>
**En complément du film, 4 formats courts éclairent des aspects précis du (télé)travail qui vient :**

####**Télétravail, écologique ?**

<iframe class="capsules" width="70%" rel="0" height="27%" src="https://www.youtube.com/embed/rTPrPW-XQIQ" title="YouTube video player" frameborder="0" rel="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


*Vanté comme une solution au réchauffement climatique, le télétravail a vite fait de se parer de vertus écologiques. Un scénario plausible ?*

<br>avec Maxime Bruggeman de la cellule mobilité de la CSC, Fanny Lederlin -  Doctorante en philosophie

####**Télétravail et non-marchand**

<iframe class="capsules" width="70%" rel="0" height="27%" src="https://www.youtube.com/embed/2XS3jcO68Bo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Dans un secteur où le lien et la présence aux publics sont au cœur des processus de travail, comment intégrer la distance sans vider le non-marchand de son essence ?*

<br>avec Véronique Van Espen - Coordinatrice de l'ABBET (association bruxelloise pour le bien-être au travail), Amik Lemaire - Directeur du Centre culturel de Jette

####**Télétravail et management**

<iframe class="capsules" width="70%" rel="0" height="27%" src="https://www.youtube.com/embed/FLnFeDTJZ_A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Vous l’avez sans doute déjà expérimenté, votre n+1 oscille entre un contrôle excessif et la gestion de votre travail en totale autonomie. Quels sont les nouveaux défis du manager de demain ?*

<br>avec Alain Piette du SPF Emploi, Véronique Van Espen, Amik Lemaire

####**Télétravail et syndicalisme**

<iframe class="capsules" width="70%" rel="0" height="27%" src="https://www.youtube.com/embed/Hk4gbdHJtnc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
*Qu’en est-il du travail syndical dès lors que les travailleurs sont chez eux, atomisés ? Quel avenir pour le combat collectif ? Sans nul doute le télétravail obligera les syndicats à inventer de nouvelles  formes de mobilisation et de lutte collectives.*

<br>avec Manon Van Thorre - service entreprises de la CSC, Sandrino Graceffa - Chercheur en Sciences sociales, Gaëlle Demez - responsable des femmes CSC


- **Réalisation** : Michel Steyaert
- **Avec la participation de** : Fatmir Blakaj, Elvira Garcia, Mathieu Godfroid, Sandrino Graceffa, Julie Hermesse, Cyprien Hoffman, Marguerite Le Galloudec, Fanny Lederlin, Philippe Michaelides, Laurent Taskin, Elisabeth Trivière, Freddy Vander Linden, Caroline Watillon
- **Image** : Hélène Motteau
- **Son** : Barbara Juniot
- **Montage image** : Salvatore Fronio
- **Montage son & mixage** : Edith Herregods
- **Étalonnage** : Maxime Tellier
- **Assistanat production** : Ken Slock
- **Production déléguée** : Louise Labib
- *À distance* est une production du [CVB - Centre Vidéo de Bruxelles](https://cvb.be/fr/films/a-distance){:target="_blank"} en coproduction l'ABBET - Véronique Van Espen, la FEC asbl – Danièle Ernotte et EGALIBERTE asbl - Sébastien Robeet

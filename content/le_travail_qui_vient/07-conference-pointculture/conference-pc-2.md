Title: Filmer le travail à l'heure de la distanciation sociale
slug: conference-pointculture-distanciation-sociale
Category: conference-pointculture
Type: conference
Template: conference
Niveau: 3
Date: 2021-05-06 10:20
Video: 

Soirée-débat à Point Culture en partenariat avec le P'tit Ciné, autour du projet documentaire « Première vague ».

Title: Soirées-débats à PointCulture Bruxelles
slug: conference-pointculture
Category: le-travail-qui-vient
Type: cycle-conferences
Template: cycle-conferences
Niveau: 3-1
Date: 2021-05-06 10:20
Video: -

nosfuturs.net et ses partenaires vous invitent à un cycle de **6 soirées-débats pour interroger le travail qui vient** : du 4 mai au 22 juin, chaque mardi soir depuis PointCulture Bruxelles et en direct sur les réseaux sociaux, **réfléchissons ensemble aux ressorts de la numérisation accélérée qui bouleverse nos habitudes et imaginaires du travail**. En résonance avec les films de nosfuturs.net, coursier·es, cinéastes, journalistes, chercheur·ses, professeur·es et étudiant·es partageront leurs pistes de réflexion et scénarios de nos futurs au travail.

###Inscription via les [évènements Facebook](https://www.facebook.com/nosfutursnet/events/){:target="_blank"}.
<br>

####Mon travail, mon enfer ? avec [Médor](https://medor.coop/){:target="_blank"}

<iframe class="capsules" width="70%" rel="0" height="27%" src="https://www.youtube.com/embed/0x4teraRAwo" title="YouTube video player" frameborder="0" rel="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
###La précarisation des travailleur·euses de plateformes numériques.
Avec **Pauline Beugnies**, réalisatrice de SHIFT ; **Philippe Engels**, journaliste pour Médor ;
**Jean-Bernard Robillard**, artiste, ex-coursier à vélo, protagoniste principal et co-auteur de SHIFT.
Modéré par **Cyril Bibas**, producteur au Centre Vidéo de Bruxelles.

####Enseignant·e, un futur numérisé ? avec [Culture & Démocratie](https://www.cultureetdemocratie.be/){:target="_blank"}

<iframe class="capsules" width="70%" rel="0" height="27%" src="https://www.youtube.com/embed/dFrwaNipTyg" title="YouTube video player" frameborder="0" rel="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
Modéré par **Renaud-Selim Sanli**, chargé de projet et de communication chez Culture & Démocratie ; avec **Tyler Reigeluth**, docteur en philosophie ; **Camille Lavier**, professeure d'arts plastiques dans le secondaire ; **Emilie Berckmans**, étudiante d'agrégation en arts plastiques.
Présenté par **Michel Steyaert**, directeur du Centre Vidéo de Bruxelles.


→ Débat en résonance avec **UNE ANNEE DE MA VIE**, un film outil de Oumar Diallo qui donne la parole aux jeunes de l'association "Notre Coin de Quartier" sur leur expérience de l'enseignement à distance.
<iframe class="capsules" width="70%" rel="0" height="27%" src="https://player.vimeo.com/video/536748323?h=f2b1575aef" title="YouTube video player" frameborder="0" rel="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

####«Notre sélection pour vous » : culture et numérisation, de la recommandation à l’algorithme. Avec la [Scam](https://www.scam.be/fr/){:target="_blank"}, la [revue Politique](https://www.revuepolitique.be/){:target="_blank"} et [Tënk](https://www.tenk.fr/){:target="_blank"}

<iframe class="capsules" width="70%" rel="0" height="27%" src="https://www.youtube.com/embed/MDreYbvDFAM" title="YouTube video player" frameborder="0" rel="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
Modéré par **Thibault Scohier** de la revue Politique ; **Renaud Maes**, chercheur (et président du Comité belge de la Scam) ; **Isabelle Rey**, cinéaste et membre du Comité belge de la Scam ; **Éva Tourrent**, responsable artistique de Tënk. Présenté par **Michel Steyaert**, directeur du Centre Vidéo de Bruxelles.

####Travailleur·euses de plateforme : la lutte pour les droits dans l’économie numérique. Avec le [Gresea](https://gresea.be/){:target="_blank"} et [Éconosphères](http://www.econospheres.be/){:target="_blank"}

<iframe class="capsules" width="70%" rel="0" height="27%" src="https://www.youtube.com/embed/qBlOFhxii5U" title="YouTube video player" frameborder="0" rel="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
Modéré par **Anne Dufresne**, chercheuse au Gresea et co-auteure de l’étude « Les travailleurs de plateforme. La lutte pour les droits dans l’économie numérique ». Avec **Martin Willems** responsable national, United Freelancers, ACV-CSC ; **Douglas Sepulchre**, coursier dans la coopérative Molembike et ex-coursier Deliveroo et **Anuar Sebban**, coursier pour diverses plateformes. Présenté par **Michel Steyaert**, directeur du Centre Vidéo de Bruxelles.


####Télétravail : les nouveaux « Temps Modernes ». Avec le [magazine Alter Échos](https://www.alterechos.be/){:target="_blank"}.

<iframe class="capsules" width="70%" rel="0" height="27%" src="https://www.youtube.com/embed/OR6kDLDTszU" title="YouTube video player" frameborder="0" rel="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
Modéré par **Pierre Jassogne**, journaliste de l'Agence Alter & auteur d’une enquête sur le télétravail dans le magazine Alter Échos du mois de juin 2021. Avec **Michel Steyaert**, réalisateur de À DISTANCE, film-outil sur le télétravail ; **Andrea Della Vecchia**, directeur du service d'étude de la centrale générale FGTB et **Mirna Vuletić**, project manager et télétravailleuse.

####Filmer le travail à l’heure de la distanciation sociale. Avec [Le P’tit Ciné](http://www.leptitcine.be/){:target="_blank"}

<iframe class="capsules" width="70%" rel="0" height="27%" src="https://www.youtube.com/embed/nQhi7sk1ddM" title="YouTube video player" frameborder="0" rel="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
Modéré par **Pauline David**, directrice du P'tit Ciné et programmatrice des rencontres Regards sur le Travail. Avec **Coline Grando**, **Olivier Magis** et **Jérôme Lemaire**, cinéastes. Présenté par **Cyril Bibas**, producteur au Centre Vidéo de Bruxelles.
<br><br>


![logo partenaires](images/logos/logos-partenaires.png){.partenaires}

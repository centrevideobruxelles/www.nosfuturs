Title: Travailleurs de plateforme : la  lutte pour les droits dans l’économie numérique 
slug: conference-pointculture-économie-numérique
Category: conference-pointculture
Type: conference
Template: conference
Niveau: 3
Date: 2021-05-06 10:20
video:

Soirée-débat à Point Culture en partenariat avec Gresea et Econosphères.

Les travailleurs de plateformes telles que Uber ou Deliveroo, représentés par des collectifs et/ou des syndicats agissent à de multiples niveaux : national, européen et international. Face au détricotage du droit du travail imposé par les plateformes hors-la-loi, et devant l’urgence de la bataille sur les statuts d’emploi, ils construisent de nouvelles formes de contestation pour la défense des droits sociaux et pour la création de nouveaux droits numériques.

Présentation : Michel Steyaert, Directeur du Centre vidéo de Bruxelles - CVB
Modératrice : Anne Dufresne, chercheuse au Gresea
Intervenants : un membre du collectif des coursiers, Luttes et revendications des coursiers en Belgique 
& d’autres invité·e·s

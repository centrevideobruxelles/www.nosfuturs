Title: "Notre sélection pour vous" Culture et numérisation, de la recommandation à l’algorithme
slug: conference-pointculture-filmer
Category: conference-pointculture
Type: conference
Template: conference
Niveau: 3
Date: 2021-05-06 10:20
video: 

Soirée-débat à Point Culture en partenariat avec la revue Politique et SCAM.

Les plateformes et le numérique en général bouleversent notre rapport à la culture, rendant des œuvres accessibles à un plus large public … ou mettant à la portée des consommateurs et consommatrices une masse infinie de contenus. Quel est l’impact de ce nouveau paradigme sur la liberté d’expérimentation et de création ? Qu’en est-il du droit d’auteur et donc des conditions de vie des créateurs et créatrices lorsque le fruit de leur travail semble gratuit et librement accessible ? Qu’est-ce que l’arrivée de ces acteurs et de leurs algorithmes change en termes de politique éditoriale, de prescription et de rapport au public ? 
Quelle règlementation devrait être mise en place, et par qui ? 
Débat en résonance avec « À scène ouverte » film collectif sur l'avenir de la scène musicale dans un monde de plus en plus virtuel. 

Présentation : Michel Steyaert, directeur du Centre vidéo de Bruxelles - CVB
Modération : Thibaut Scohier, revue Politique
Intervenant·es : Renaud Maes, chercheur (et président du Comité belge de la Scam), un.e cinéaste et membre du Comité belge de la Scam, Un·e représentant·e d’une plateforme VOD

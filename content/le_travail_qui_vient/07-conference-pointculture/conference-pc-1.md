Title: Mon travail, mon enfer ?
slug: conference-pointculture-1
Category: conference-pointculture
Type: cycle-conferences
Template: cycle-conferences
Niveau: 3
Date: 2021-05-06 10:20
Video: 

Soirée-débat à Point Culture en partenariat avec le média belge d'investigations Médor, autour *du combat des coursiers face aux conditions précaires de travail et au projet de société du capitalisme de plateformes*.

Présentation : Cyril Bibas, producteur au Centre vidéo de Bruxelles - CVB
Modérateur : Philippe Engels, journaliste et coordinateur de l’Appel des 100 (pour un travail décent)
Intervenants : Pauline Beugnies, réalisatrice de SHIFT et auteure de l'article "Deliveroo du mal" édité dans Médor, magazine n°22 ; Jean-Bernard Robillard, artiste, coursier à vélo, protagoniste principal et co-auteur de SHIFT.
 

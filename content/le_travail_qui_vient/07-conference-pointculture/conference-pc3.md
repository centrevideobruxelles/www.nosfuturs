Title: Enseignant·e, un futur numérisé ?
slug: conference-pointculture-enseignant
Category: conference-pointculture
Type: conference 
Template: conference 
Niveau: 3 
Date: 2021-05-06 10:20
Video: 

Soirée-débat à Point Culture en partenariat avec Culture & Démocratie.

À l'heure du confinement, la continuité de l'enseignement a été assurée par le biais numérique, dans la suite "logique" d'un processus en cours depuis longtemps. Cette numérisation de l'enseignement, présentée comme une évidence technique, mérite d'être questionnée à l'aune de ses fantasmes et réalités.

Présentation : Michel Steyaert, directeur du Centre vidéo de Bruxelles - CVB
Moderateur : Renaud-Selim Sanli Chargé de projets et de communication chez Culture & Démocratie
Intervenants :Tyler Reigeluth, Docteur en philosophie ; un·e prof dans le secondaire ; un·e étudiant·e en fin de cycle 

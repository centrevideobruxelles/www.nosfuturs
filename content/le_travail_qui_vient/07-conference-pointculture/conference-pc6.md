Title: Télétravail : les nouveaux "Temps modernes" ?
slug: conference-pointculture-teletravail
Category: conference-pointculture
Type: conference
Template: conference
Niveau: 3
Date: 2021-06-08 10:20
Video: 

Soirée-débat à Point Culture en partenariat avec l'agence Alter.

Présentation : Centre vidéo de Bruxelles
Modérateur : Pierre Jassogne, journaliste de l'Agence Alter & auteur d’une l'enquête à venir dans Alter échos
Intervenants : Michel Steyaert, réalisateur de A DISTANCE film-outil sur le télétravail 

Title: Édito: Le travail qui vient
Slug: le-travail-qui-vient
Template: item_projet
Date: 2010-12-03 10:20
Modified: 2010-12-05 19:30
Category: theme
Niveau: 2
Summary: Short version for index and feeds
Tags: uberisation, algorithme, ranking, economie de plateforme, flexijob, data, teletravail
Status: published

Déployée sur 2 ans, la thématique « Le travail qui vient » portera en 2021-2022 sur l’atomisation du travail (VOLET 1) et en 2022-2023 sur les organisations collectives du travail (VOLET 2).

Alors que la pandémie a eu comme effet, dans nos sociétés, de généraliser le recours au télétravail et avec lui l’usage exponentiel et individualisé des applis ou interfaces mises en place par des plateformes telles qu’Amazon, Netflix, Uber ou Deliveroo… Alors que le développement accéléré de l’économie numérique change les modalités d’échanges et d’intermédiation entre fournisseurs et demandeurs de services, mais aussi entre travailleurs... le premier volet de nosfuturs.net s’intéresse à l’**atomisation du travail**.

Comme le pointe la sociologue Dominique Méda [^1], *« à l’heure du développement de l’économie numérique et de l’efficacité des algorithmes, le scénario de la révolution technologique en marche semble particulièrement bien s’accommoder d’un démantèlement des protections du travail et de l’emploi encore en vigueur en Europe ».*

### Dès lors à quoi ressembleront les emplois de demain ?

Le modèle des plateformes digitales, qui découpe le travail en prestations individualisées, n’accentue-t-il pas de ce fait l’explosion des collectifs et l’individualisation des relations de travail ? Que se cache-t-il derrière les notions de liberté et de flexibilité promues par  Uber ou Deliveroo ? Comment faire valoir ses droits face à un algorithme qui se substitue à la figure traditionnelle de l’employeur ? Le travail indépendant ou à la tâche va-t-il prendre le pas sur le salariat ?
La numérisation et l’automatisation sont-elles compatibles avec le respect de nos acquis sociaux ?

Autant de questions et de possibles scénarios que nosfuturs.net vous invite à éprouver et à penser dès aujourd’hui et pour demain.

[^1]: Dominique Méda dans L’avenir du travail : sens et valeur du travail en Europe / Document de recherche de l'OIT n°18  - décembre 2016

Title: Curriculum Futuræ
slug: curriculum-futurae
Category: le-travail-qui-vient
Type: podcast
Template: podcast
Niveau: 3
Date: 2010-12-03 10:20
Cover: 03-curriculum-futuræ/global.jpg


Construit comme une enquête, **Curriculum Futuræ** nous projette dans le futur. Une auto-fiction documentaire au ton à la fois réaliste et décalé dont les 4 épisodes abordent divers scénarios du travail de demain

Ce podcast s’adresse à une génération à la fois friande de nouvelles applis, commandant parfois une pizza sur Uber ou des livres sur Amazon, mais consciente que quelque chose ne tourne pas rond au royaume de la consommation à coups de clics sur fond de GAFAM.

Cette série est écrite et réalisée par **Sebastian Dicenaire.** Ses précédentes créations sonores s’intéressent de près aux dérives de la technologie : dans *John Haute Fidelité*, il aborde la collecte des données personnelles et du BigData ; dans *DreamStation*, il évoque l’hyper-uberisation du monde du travail et du marketing à outrance qui peuple désormais nos rêves...
Ses fictions radiophoniques ont été récompensées par plusieurs prix et largement partagées sur les réseaux sociaux en France et en Belgique.

<html><details><summary>Sebastian Dicenaire</summary>
Né à Strasbourg en 1979, bruxellois depuis 2001, Sebastian Dicenaire est poète et réalisateur de fictions radiophoniques. Il utilise le langage dans tous ses états : papier, performance, création radio, vidéo, dessin…
Il a publié plusieurs ouvrages de poésie (Döner-kebab, éd. Héros-Limite ; Personnologue, éd. Clou dans le fer ; Dernières Nouvelles de l’Avenir, éd. Atelier de l'agneau… ).
Sur scène, son travail cherche à repousser les limites de l’imagination à travers la création d’un « cinéma mental » mêlant textes et bandes-son.
Ses fictions radio, mêlant poésie et science-fiction, mythologie et technologie, sont diffusées principalement par la RTBF et ont été primées dans de nombreux festivals (Prix SACD Belgique, Prix Phonurgia Nova, Prix SGDL, Prix Europa…).
Il a créé des podcasts d’anticipation pour France Culture (DreamStation en 2019 autour des rêves) ou la RTS (Clinique de la Mémoire Morte en 2020 autour de la mémoire), et réalise actuellement un podcast cherchant à documenter l’avenir du travail, Curriculum Futurae.
</details></html>

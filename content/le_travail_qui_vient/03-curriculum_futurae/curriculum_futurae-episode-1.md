Title: Mes enfants, Uber et moi! <sup>[1/4]</sup>
slug: curriculum-futurae-1
Category: curriculum-futurae
Type: episode-podcast
Template: episode-podcast
Niveau: 2
Date: 2010-12-03 10:20
Liens: lexique_algorithme_7 lexique_algorithme_4 lexique_uberisation_3 lexique_microtravail_2 lexique_economie-de-plateforme_2 lexique_flexijob conference-pointculture-économie-numérique article-adefinir
Audio: https://feeds.soundcloud.com/stream/997711588-nosfuturs_net-algorithme.mp3
cover: 03-curriculum-futuræ/01.jpg

À l'heure de l’uberisation, de la robotisation des tâches et des algorithmes auto-apprenants, un père trentenaire un brin flippé s'inquiète de l'avenir professionnel de ses jumeaux de 5 ans... À la rencontre de spécialistes de tous bords, il mène l'enquête sur les **scénarios futurs** du travail.

- **Écriture, réalisation et montage** : Sebastian Dicenaire
- **Prise de son, montage et mixage**  : Aurélien Lebourg
- **Sound design** : Anne Lepère
- **Musique originale** : Fièvre (en collaboration avec Thierry Bodson)
- **Illustration** : Morgane Somville
- **Production déléguée** : Cyril Bibas & Marc Jottard – CVB
- **Direction de production** : Joël Curtz – CVB
- **Avec (par ordre d’audition)** :  Dominique Méda, Antoinette Rouvroy, Jean-François Neven, Laurent Wartel, Jean-Gabriel Ganascia, Mark Hunyadi, Natacha Dupont, Fiona Grau, Christine Stanczyk et Cathy Van Remoortere
- **Les fées** : Sybille Cornet, Florence Klein, Vincent Tholomé
- **Coach chœur** : Maja Jantar
- **Coach Design Fiction** : Flora Six
- **Prise de son additionnelle** : Arnaud Marten, Edith Herregods, Pierre Devalet
- *Curriculum futurae* a reçu l’aide du Fonds d'aide à la création radiophonique (FACR) et du fonds Gulliver. La série est coproduite par l’[ACSR](http://www.acsr.be/a-propos/){:target="_blank"} - atelier d’accompagnement à la réalisation de récits sonores radiophoniques, soutenu par le Fonds d’Aide à la Création Radiophonique de la Fédération Wallonie-Bruxelles. Curriculum Futurae est une production du [CVB - Centre Vidéo de Bruxelles](https://cvb.be/fr/films/curriculum-futurae){:target="_blank"}

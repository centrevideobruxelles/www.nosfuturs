Title: Algorithmes mélodies <sup>[2/4]</sup>
slug: curriculum-futurae-2
Category: curriculum-futurae
Type: episode-podcast
Template: episode-podcast
Niveau: 2
Date: 2010-12-03 10:20
Audio: https://feeds.soundcloud.com/stream/1045560373-nosfuturs_net-cf-ep2-mix-8mai.mp3
cover: 03-curriculum-futuræ/02.jpg
bio: Et si demain tout venait à s'automatiser, la technologie rendrait-elle le travail plus désirable ?
imgbio: 03-curriculum-futuræ/seb.jpg

Et si demain tout venait à s'automatiser, la technologie rendrait-elle le travail plus désirable ?

- **Écriture, réalisation et montage** : Sebastian Dicenaire
- **Prise de son, montage et mixage**  : Aurélien Lebourg
- **Sound design** : Anne Lepère
- **Musique originale** : Fièvre (en collaboration avec Thierry Bodson)
- **Illustration** : Morgane Somville
- **Production déléguée** : Cyril Bibas & Marc Jottard – CVB
- **Direction de production** : Joël Curtz – CVB
- **Avec (par ordre d’audition)** :  Dominique Méda, Antoinette Rouvroy, Jean-François Neven, Laurent Wartel, Jean-Gabriel Ganascia, Mark Hunyadi, Natacha Dupont, Fiona Grau, Christine Stanczyk et Cathy Van Remoortere
- **Les fées** : Sybille Cornet, Florence Klein, Vincent Tholomé
- **Coach chœur** : Maja Jantar
- **Coach Design Fiction** : Flora Six
- **Prise de son additionnelle** : Arnaud Marten, Edith Herregods, Pierre Devalet
- *Curriculum futurae* a reçu l’aide du Fonds d'aide à la création radiophonique (FACR) et du fonds Gulliver. La série est coproduite par l’ACSR, atelier d’accompagnement à la réalisation de récits sonores radiophoniques, soutenu par le Fonds d’Aide à la Création Radiophonique de la Fédération Wallonie-Bruxelles. Curriculum Futurae est une production du [CVB - Centre Vidéo de Bruxelles](https://cvb.be/fr/films/curriculum-futurae){:target="_blank"}

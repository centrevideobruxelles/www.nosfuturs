Title: Medor - Deliveroo nous du mal
slug: deliveroo_nous
Category: le-travail-qui-vient
Type: texte
Niveau: 2-1
Date: 2010-12-03 10:20
Liens : shift

![Bihua Yang](https://medor.coop/media/images/dessin-grand-1-elargi.max-800x530.png)
<span> Article publié dans Medor N°22 - Texte: Pauline Beugnies, Illustration: Bihua Yang.</span>

Il a parcouru 20 000 kilomètres à vélo, un sac carré turquoise sur le dos. L’application Deliveroo lui donnait des missions. Comme un patron de poche. Jean-Bernard devait-il, dès lors, bénéficier d’un contrat d’employé ? Le statut de cet ancien livreur déchaîne la justice et interroge les conditions de travail dans la nouvelle économie de plateformes.

Jean-Bernard Robillard est un artiste français, installé en Belgique. Il enchaîne les petits boulots et les emmerdes. Il a 38 ans et ses parents paient son loyer pour ne pas qu’il se retrouve à la rue. Fin 2015, il s’inscrit sur l’application de Take Eat Easy, une start-up belge de livraison de repas à vélo. Il peut commencer dès le lendemain. Il a juste besoin d’un vélo et d’un smartphone. Son voisin lui prête un VTT orange rafistolé. Il n’est plus monté sur un vélo depuis 15 ans et fume trois paquets de cigarettes par jour…

« Il fallait être bourré pour ne pas réussir la Ghost Ride (course test, NDLR). Ils prenaient vraiment tout le monde. » Pour sa première course, Jean-Bernard est envoyé à Evere. Il pleut, il vente. Il n’est pas équipé. Il ne sait pas où il va, son téléphone dans une main, son guidon dans l’autre. Il rentre chez lui à 23 heures, grelottant. Ce soir-là, il fait le compte : il a livré quatre commandes et gagné 30 euros brut.

Au bout d’une semaine, Jean-Bernard a déjà été heurté deux fois ! Il investit dans un casque. « J’avais mal au cul. J’ai dû acheter de la crème anti-hémorroïdaire. Mais je savais que j’étais mieux là, six heures par jour sur mon vélo, que chez Total où j’avais bossé un peu avant avec une pression psychologique infernale… Même si tu ne gagnes pas grand-chose, tu es libre sur ton vélo, tu t’évades, tu vois pas le temps passer. »

Quelques mois plus tard, il apprend que chez Deliveroo les livreurs sont payés à l’heure. Ils ont mauvaise réputation : « On les prenait de haut, c’étaient pas des “vrais”. Nous on savait rouler. On prenait des ris­ques. Mais, bon, je ne gagnais que 800 euros par mois alors que je roulais au moins 35 heures par semaine. » Jean-Bernard rejoint Deliveroo.

En juillet 2016, Take Eat Easy fait faillite. La start-up ne remboursera jamais les 400 000 euros avancés par la Smart (intermédiaire) aux coursiers laissés sur le carreau.

##Maillot à pois

Finalement, Jean-Bernard se sent bien dans la boîte, qui valorise la « culture coursier » et organise un tas d’événements pour ses livreurs (fêtes, matchs de foot…). Jean-Bernard tutoie son patron. Il devient l’un des coursiers réguliers, avec plus de 1 000 kilomètres parcourus par mois pour 1 euros net. C’est la première fois de sa vie qu’il est salarié à temps plein.

À l’époque, 80 % des coursiers de Deliveroo travaillent comme salariés, grâce à une convention collective de travail signée avec la coopérative Smart. Les minimums légaux sont respectés et ils bénéficient d’une protection sociale. Alors, le jour où Jean-Bernard se casse une dent en tombant de son vélo, ses frais médicaux sont entièrement pris en charge.

Très vite, Jean-Bernard bat tous les records de livraison. Il gagne de nombreuses fois le « maillot à pois » aux couleurs de Deliveroo, une récompense censée encourager les coursiers à se dépasser. « Aujourd’hui, malgré tout ce qu’il s’est passé ensuite, j’en suis encore assez fier. »

La société britannique Deliveroo devient l’une des rares sociétés européennes à accéder au statut de « licorne » qui désigne les start-up valorisées à au moins un milliard de dollars et encore non cotées en Bourse.

En 2017, Jean-Bernard participe, sponsorisé par Deliveroo, au Championnat d’Europe des livreurs à vélo à Vienne. Cette même année, « Louis », le programme d’attribution des commandes aux coursiers, est remplacé par le logiciel « Franck ». Celui-ci géolocalise les coursiers connectés, leur propose des courses, calcule leur itinéraire et, ensuite, leurs revenus. Basé sur une technologie prédictive, ce nouvel algorithme plus performant permet de réduire les temps de livraison.

Les livreurs reçoivent régulièrement « leurs statistiques » : total des commandes livrées, total des commandes refusées, temps pour accepter une commande, vitesse, kilomètres. Le message se termine sur ces mots : « Bonne continuation. Love&Bicycle. L’équipe Deliveroo. » Malgré ces mots doux, l’ambiance change chez Deliveroo. Plus de matchs de foot, plus de maillot à pois.

##Super-flexibles ou super-pigeons ?

En juillet de la même année, le service clients de Deliveroo Benelux, chargé des contacts avec les coursiers, les restaurateurs et les clients, est déplacé à Madagascar. Huit personnes sont licenciées à Bruxelles. Le lien entre les coursiers et le siège est rompu. « Si on avait un pneu crevé ou si personne ne répondait à la sonnette, c’est Madagascar qu’on devait appeler. Et on n’avait pas le droit de connaître le prénom de la personne qui nous aidait, alors que même l’algorithme avait un nom, “Franck” ! » Jean-Bernard se dit que quelque chose ne tourne pas rond.

En novembre 2017, Deliveroo rompt unilatéralement la convention de travail avec la Smart, sans prévenir les coursiers. Ceux qu’elle appelle désormais ses « collaborateurs coursiers » seront indépendants ou étudiants entrepreneurs, rémunérés non plus à l’heure mais à la commande. S’ils ne sont pas d’accord, ils devront quitter l’entreprise. L’ultimatum est fixé au 1er février 2018. Jean-Bernard se sent trahi et rejoint le collectif des coursiers créé quelques mois plus tôt. « J’ai compris qu’il fallait se mobiliser. »

Pour expliquer ce changement de cap, le directeur Benelux de Deliveroo, Mathieu de Lophem, invoque une volonté d’offrir plus de liberté à ses livreurs. D’après des enquêtes menées par Deliveroo, ceux-ci souhaiteraient « organiser leur travail autour de leur vie », plutôt que l’inverse.

Des coursiers réguliers, parmi lesquels Jean-Bernard, tentent d’obtenir de Mathieu de Lophem un minimum horaire garanti. Sans succès. Le patron défend une idée de « flexisécurité ». Les coursiers s’interrogent sur le fonctionnement de l’algorithme. Ils suspectent aussi que, pendant ce conflit social, l’algorithme favorise les indépendants et les étudiants pour rendre plus attractif ce nouveau statut. Mathieu leur assure que non, mais il ne peut pas leur dire comment ça fonctionne. Il fait confian­ce à l’équipe « tech » : « C’est inexplicable, car c’est incompréhensible. J’ai abandonné au bout de 12 secondes. » Un tribunal italien jugera d’ailleurs plus tard, en décembre 2019, que les critères de participation et de fiabilité (ceux qui refusent plus de courses sont considérés comme moins fiables) pris en compte par Franck, l’algorithme de Deliveroo, sont discriminatoires.

Pendant la mobilisation, Jean-Bernard est tiraillé. Il a envie de sauver son boulot et de faire confian­ce à la direction, mais il ne peut accepter ce changement de statut.

![illu2](https://medor.coop/media/images/dessin-3.max-800x600.png)
<span>Bihua Yang. CC BY-NC-ND.</span>

##C’est un licenciement par déconnexion.

En janvier 2018, alors que le conflit social s’intensifie et que la date fatidique approche, Deliveroo obtient son agrément fédéral en tant que « plateforme numérique de l’économie collaborative ». Dorénavant, jusqu’à un certain plafond [^1], les coursiers qui travaillent sous ce régime fiscal bénéficieront d’un taux de taxation réduit, ne paieront pas de TVA ni de cotisation sociale. S’ils dépassent le plafond, ils seront alors considérés comme indépendants et soumis à une régularisation fiscale. En s’intégrant dans ce cadre, connu aussi sous le nom de « loi De Croo » ou « P2P », Deliveroo tente d’évacuer la question du choix entre les statuts de salarié et d’indépendant qui l’oppose au collectif des coursiers, soutenu par la Centrale nationale des employés (CNE). La start-up vend ainsi à ses coursiers un régime de travail ultra-attractif et super-flexible. Le rêve. Mais ce régime fiscal n’est pas un statut de travail et donc, contrairement au statut d’indépendant, n’offre pas de protection sociale ni de droit du travail.

Jean-Bernard, l’ancien maillot à pois de Deliveroo, devient porte-parole des coursiers. Du jour au lendemain, Douglas, l’un des membres fondateurs du collectif, réalise qu’il n’a plus accès à la plateforme. Il peut dire adieu à son boulot. C’est un licenciement par déconnexion. Malgré ce risque, les coursiers enchaînent les actions. Un samedi soir de janvier 2018, ils parviennent, avec le soutien de quelques restaurateurs qui éteignent l’application pour la soirée, à fermer une zone de livraison à Bruxelles.

Quelques jours avant l’ultimatum pour le changement de statut, Douglas et d’au­tres décident d’occuper les bureaux de Deliveroo pour forcer les patrons à négocier. Jean-Bernard n’est pas pour. « Je trouvais ça trop violent pour les employés du bureau. » Le pari du collectif fonctionne. Les coursiers, accompagnés des syndicats, sont reçus au ministère du Travail pour une médiation. Le ministre Peeters n’est pas présent. Il est à Davos au Forum économique mondial, où il rencontre la vice-présidente de Deliveroo International, Thea Rogers. Dans une dépêche de l’agence Belga, on peut lire que, selon un porte-parole de Deliveroo, la réunion a été « constructive », et celui-ci se félicite que le ministre ait condam­né les actes qui ont eu lieu au siège, « visant à intimider le personnel de Deliveroo ». À Bruxelles, le chef de cabinet de Kris Peeters assure aux coursiers et à la CSC qui les accompagne qu’ils prennent le problème à bras-le-corps et qu’une enquête de l’auditorat du travail et de l’ONSS sur le statut social des coursiers est en cours.

<blockquote>
« Il y aura toujours quelqu’un dans la dèche pour travailler pour ces plateformes »
<span>Jean-Bernard</span>
</blockquote>

Les négociations au ministère du Travail se soldent par un échec. Deliveroo promet à chaque coursier un casque et une lampe. Jean-Bernard se voit proposer une solution sur mesure pour lui et les quelques réguliers, en off. Il est indigné. « La veille de l’ultimatum, j’ai accepté quelques commandes que je n’ai pas livrées… Dernier acte de résistance désespéré… Je réalisais, horrifié, que le salariat, que je venais à peine de découvrir, n’existerait bientôt plus… Pour Deliveroo, coursier, ce n’est pas un métier, c’est comme les loueurs d’Airbnb, sauf que, nous, on investit dans nos vélos et on loue nos corps ! » Le 1er février 2018, 600 coursiers, dont Jean-Bernard, quittent Deliveroo.

Jean-Bernard a suffisamment cotisé comme coursier salarié pour avoir droit au chômage. Pour la première fois de sa vie, il touche des allocations. « Sans cette sécurité, j’aurais sans doute accepté le nouveau statut précaire imposé par Deliveroo. Je pense à mes potes qui n’ont pas eu cette chance. Elle est là, leur puissance : il y aura toujours quelqu’un dans la dèche pour travailler pour ces plateformes. »

L’ancien coursier n’en reste pas là. Sous l’impulsion de Martin Willems, syndicaliste de la CNE, Jean-Bernard découvre l’existence de la Commission Relation Travail (CRT). C’est une instance au sein du SPF Sécurité sociale dont le rôle est de déterminer la nature d’une relation de travail (salarié ou indépendant) en vertu de quatre critères principaux (volonté des parties, liberté d’orga­niser le travail, liberté d’organi­ser le temps de travail, possibilité d’exercer un contrôle hiérarchique). Cette procédure est gratuite et rapide, contrairement à une démarche devant le tribunal du travail pour requalifier une relation de travail.

La CRT se prononce : si Jean-Bernard voulait reprendre son travail de coursier pour Deliveroo, il devrait le faire sous un contrat de travail salarié. La CRT a notamment remis en cause la prétendue liberté des coursiers d’orga­niser leur temps de travail, à cause du lien de subordination exercé par la plateforme (et donc par un logiciel). Une victoire d’ordre symbolique pour les coursiers. C’est la première fois en Belgique qu’une institution se prononce sur le statut social des coursiers à vélo. Deliveroo fait appel.

##Visite d’huissier

Deux mois plus tard, fin avril 2018, un coup de sonnette strident fait sursauter Moule-Frite, le chat de Jean-Bernard. C’est un huissier qui remet à Jean-Bernard une citation à comparaître, aux côtés de l’État belge, devant le tribunal du travail de Bruxelles. Deliveroo l’attaque en justice. L’entreprise veut faire invalider l’avis de la CRT et reconnaître par un juge que la relation de travail qui l’unit à ses coursiers relève bien du statut d’indépendant. Jean-Bernard panique. Il risque d’être condamné à payer les frais d’avocat et de procédure.

Il est syndiqué depuis peu et la CSC le soutient. Son avocate, Sophie Remouchamps, spécialisée dans le droit du travail et de la sécurité sociale, se demande s’il est bien normal de se retrouver au tribunal après avoir demandé l’avis de la CRT. « La procédure est gratuite, simple et rapide. Plus personne ne sollicitera cette commission si la conséquence est un éventuel procès », déplore l’avocate. Ce procès excite beaucoup les juristes, car c’est la première fois en Belgique que cette question de la subordination exercée par un logiciel est posée.

<blockquote>
  « Cet état de fait impose d’innombrables vides juridiques. »
  <span>Bernard Stiegler, philosophe</span>
</blockquote>

Entre-temps, les livreurs ne passent déjà quasiment plus par le statut d’indépendant. En Belgique les coursiers travaillent dorénavant quasiment tous dans le cadre de la loi de l’économie collaborative, dite « loi De Croo ». On est en plein dans la disruption, telle que décrite par le philosophe Bernard Stiegler [^2]. « La disruption consiste, de la part des seigneurs de la guerre économique, à aller plus vite que les sociétés pour les soumettre à des modèles qui détruisent les structures sociales et paralysent la puissance publique. Cet état de fait impose d’innombrables vides juridiques. »

![illu3](https://medor.coop/media/images/dessin-2.max-800x600.png)
<span>Bihua Yang. CC BY-NC-ND.</span>

En Europe, rien qu’en 2017, 40 mobilisations de livreurs (grèves, actions symboliques, manifestations,…) ont eu lieu. Jean-Bernard ne travaille plus comme livreur mais est très actif dans le collectif. En octobre 2018, il participe à Bruxelles à la première assemblée générale européenne des coursiers. Le sommet réunit plus de soi­xante coursiers de plateformes de livraison à domicile comme Deliveroo, Glovo et autre Uber Eats,… Ils créent la Fédération transnationale des coursiers. Une charte de revendications est établie, dont deux se dégagent, le minimum horaire garanti et la transparence des données utilisées par les plateformes. « Il y avait cette volonté de revendiquer ensemble. Et la nécessité sans doute aussi. Les plateformes ne s’embarrassent pas des frontières. »

En juillet 2019, le tribunal du travail invalide l’avis de la CRT. Non pas pour des questions de fond, mais pour une question de procédure : la commission n’était pas habilitée à se prononcer sur un dossier qui faisait l’objet d’une enquête de l’auditorat du travail. C’est alors au tour de Deliveroo « d’utiliser » les médias en sa faveur. Son porte-parole, Rodolphe Van Nuffel, déclare à l’agence Belga : « La décision de la CRT est invalide et doit être annulée. » On peut ensuite lire ce titre trompeur dans la DH : « Le tribunal a tranché, les travailleurs de Deliveroo ne sont pas des salariés. » L’audience est reportée au mois de septembre 2021.

##Business de crise

Une enquête de l’auditorat du travail, réalisée auprès d’une centaine de coursiers, sort en décembre 2019. Elle révèle des infractions à la législation sociale de la part de Deliveroo, comme le non-paiement de cotisations à l’Office national de la sécurité sociale (ONSS). L’auditorat du travail assigne à son tour la start-up britannique. Les coursiers peuvent se joindre au procès contre Deliveroo pour voir leur statut régularisé en statut de salarié (pour toutes les heures prestées antérieurement aussi) si le juge suit l’avis de l’ONSS.

Jean-Bernard continue de travailler pour le collectif. Il va de temps en temps à la rencontre des coursiers dans des lieux où ils attendent les commandes. Il essaye de les encourager à se mobiliser, face à des conditions de travail de plus en plus précaires. Le régime P2P et sa limite annuelle ont encouragé un marché de faux comptes. Certains en situation illégale ou ayant atteint le plafond achètent de faux comptes jusqu’à 1 000 euros pour travailler. « Les coursiers que je rencontre aujourd’hui sont rarement en mesure de se mobiliser, leur situation est soit illégale, soit trop précaire. Ils sont rarement au courant de leurs droits. » Bonne nouvelle : en avril 2020, la Cour constitutionnelle a invalidé ce régime.

Jean-Bernard est aujourd’hui gestionnaire de projets à la Smart. Il travaille sous contrat d’employé. Pendant la crise du Covid, les chiffres des plateformes de livraison ont explosé. Deliveroo vaut aujourd’hui 7 milliards et se prépare à entrer en Bourse. Confiné, Jean-Bernard dénonce un business de la crise. Dans une lettre, publiée dans Le Soir le 24 novembre 2020, il s’adresse directement au Premier ministre Alexander De Croo. « Devons-nous rappeler que vous êtes à l’origine d’une des lois les plus controversées (loi De Croo) qui a permis à des entreprises comme Deliveroo, Uber Eats (et bientôt Colruyt ?) de se dédouaner totalement de leur responsabilité d’employeur envers plusieurs milliers de coursiers depuis 2016 et donc de ne payer aucune charge patronale ni cotisation sociale, pourtant, ô combien nécessaires pour la solidarité nationale dont vous vous faites le chantre maintenant ? »

Pourquoi ce courrier si la Cour constitutionnelle avait tout annulé ? Le 20 novembre, le gouvernement a approuvé une loi fiscale réhabilitant l’ancienne loi De Croo. Et le travail hyperflexible qui va avec. Pour le bien des coursiers, évidemment.

[^1]: Fixé à l’époque à 5 100 ­€ par an, à 6 340 € en 2020.
[^2]: Bernard Stiegler, Dans la disruption. Comment ne pas devenir fou ?, Les Liens qui libèrent.

- Illustrations (CC BY-NC-ND) : Bihua Yang
- Textes (CC BY-NC-ND) : Pauline Beugnies
- Publié le 04/03/2021

Title: Colophon
slug: colophon
Template: colophon
Date: 2010-12-03 10:20
Modified: 2010-12-05 19:30
**nosfuturs.net est une plateforme low-tech créée et produite par** :</br>le CVB - Centre Vidéo de Bruxelles, association dirigée par Michel Steyaert

**Coordination générale et éditorialisation** :</br>Cyril Bibas & Marc Jottard

**Promotion et diffusion** :</br>Philippe Cotte, Florence Peeraer & Alice Riou

**Design et développement** :</br>[Luuse](http://www.luuse.io){:target="_blank"}

#### À propos de ce site

Derrière ses airs animés, ce site est statique.</br>
Il a été réalisé grâce [Pelican](https://blog.getpelican.com){:target="_blank"}</br>
Son contenu a été injecté via [GitLab](https://gitlab.com){:target="_blank"}.</br>
Il aura fallu 619 commits pour aboutir à sa mise en ligne,</br>
nosfuturs.net ne fait appel à aucune bibliothèque JavaScript</br>
Il est bâtit en HTML et décoré en CSS, soupoudré d’une pointe de pure JavaScript.</br>
Les typographies utilisées sont la [HKGrotesk](https://hanken.co/products/hk-grotesk){:target="_blank"} d’Hanken Design Co. et d’Alfredo Marco Pradi ainsi que la [Redaction](https://www.redaction.us){:target="_blank"} de Titus Kaphar et Reginald Dwayne Betts</br>
Il est hébergé par [Domaine Public](https://www.domainepublic.net){:target="_blank"} à Bruxelles</br>
nosfuturs.net n'utilise pas de cookies.

#### Partenaires
l'Agence Alter, Culture & Démocratie, Econosphères, le Gresea, Le P’tit Ciné, le magazine Médor, PointCulture, la revue Politique & la SCAM

#### Centre Vidéo de Bruxelles

- 111 rue de la Poste
- 1030 Bruxelles
- T: +32 (0)2 221 10 50
- [info@nosfuturs.net](mailto:info@nosfuturs.net)
- [info@cvb.be](mailto:info@nosfuturs.net)


![logo FWB](images/logos/FWB_transparent.png)
![logo cocof](images/logos/cocof.png)
![logo CVB](images/logos/CVB_mini.png)

© nosfuturs.net - CVB tous droits réservés 2021

Title: Contact
slug: contact
Template: apropos
Date: 2010-12-03 10:20
Modified: 2010-12-05 19:30
Summary: description home
Status: published

Le **Centre Vidéo de Bruxelles asbl**, association pluraliste fondée en 1975, est un Atelier de production de films documentaires et une Association d’éducation permanente, reconnus tous deux par la Fédération Wallonie Bruxelles. Le CVB est également un opérateur audiovisuel au service du secteur associatif sur le territoire de la Région de Bruxelles-capitale et est reconnu à ce titre par la Commission communautaire française.

Centré essentiellement sur les réalités sociales, politiques et culturelles de son temps, le CVB accompagne des projets d'auteur·e·s dans le champ du cinéma documentaire, accueille des projets audiovisuels issus du secteur associatif et organise des ateliers vidéo participatifs et collectifs avec différents types de publics.

Attentif depuis sa création à la forme et au langage, le CVB encourage l’émergence de nouvelles formes d’écriture et soigne particulièrement le travail formel dans toutes ses productions, qu’elles naissent dans le champ du cinéma ou du côté de l’associatif.

C’est dans cet esprit et pour coller au plus près de l’évolution des modes de narration et des espaces de diffusion que le CVB a initié en 2019 un véritable travail de recherche dans le domaine du transmédia dont le premier résultat concret est nosfuturs.net.



[info@nosfuturs.net](mailto:info@nosfuturs.net)

[info@cvb.be](mailto:info@nosfuturs.net)

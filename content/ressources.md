Title: Ressources
slug: ressource
Category: le-travail-qui-vient
Template: ressources
type: ressource
Niveau: 3-4
Date: 2010-12-03 10:20
Modified: 2010-12-05 19:30

#### ressources partenaires
- ressources autres

## Technologie et travail :  quels futurs en communs ?

### La digitalisation accrue des flux de données et des transactions créent de nouvelles possibilités, mais aussi de nouveaux raccourcis et rapports de forces dans le monde du travail… Dans l’ombre des colosses de l’économie de plateforme et de leurs puissants algorithmes, c’est la question du contrôle exercé à travers la technologie et sur elle, qui est engagée...

- [L’association Algorithmwatch](https://algorithmwatch.org/en/){:target="_blank"} observe et analyse l’impact des processus de décision algorithmique dans tous les domaines, notamment celui du travail.

#### Menacés par la robotisation et l’automatisation, de nombreux emplois humains disparaîtraient d’ici vingt ans. Une enquête d'Alter Échos sur le monde du travail de demain, pour interroger les enjeux de la révolution annoncée : [Le rire jaune de la caissière](https://www.alterechos.be/4emerevolution/episode3){:target="_blank"}

- ["Les algorithmes organisent massivement l’autoexploitation."](https://www.jefklak.org/les-algorithmes-organisent-massivement-lautoexploitation/){:target="_blank"} Le droit du travail à l’épreuve de l’uberisation. Un entretien avec Barbara Gomes, juriste et membre du collectif Pédale et tais-toi. Par Mickaël Correia, membre du collectif Jefklak

- [Auditer les algorithmes de la difficulté de passer des principes aux applications concrètes]( https://www.internetactu.net/a-lire-ailleurs/auditer-les-algorithmes-de-la-difficulte-de-passer-des-principes-aux-applications-concretes/){:target="_blank"} – Hubert Guillaud.

- [Chez Uber ou Deliveroo, vous serez toujours face à un algorithme opaque](https://www.telerama.fr/idees/chez-uber-ou-deliveroo-vous-serez-toujours-face-a-un-algorithme-opaque-6759123.php){:target="_blank"}, un entretien d’Olivier Tasquet avec l’économiste Odile Chagny.

#### [À l'assaut du langage](https://medor.coop/magazines/medor-8-autumn-2017/a-lassaut-du-langage/){:target="_blank"}, un article de Médor sur les coder Dojos qui forment nos jeunes à triturer du code informatique, car notre pays manquent d'informaticiens.

- [« Les réseaux sociaux hébergent les pires immondices dont sont capables des humains »](https://larevuedesmedias.ina.fr/reseaux-sociaux-moderateurs-web-sarah-t-roberts){:target="_blank"}, un entretien avec la chercheuse Sarah T. Roberts, proposé par la Revue des Médias  de l’INA.

#### [La gouvernementalité algorithmique en trois questions](https://www.pointculture.be/magazine/articles/focus/gouvernementalite-algorithmique-3-questions-antoinette-rouvroy-et-hugues-bersini/){:target="_blank"}, un entretien avec Antoinette Rouvroy et Hugues Bersini.

#### [PointCulture TV propose également une conférence d’Antoinette Rouvroy : « Pour un numérique humain et critique »](https://www.youtube.com/watch?v=VHtyGxVuhN4){:target="_blank"}

#### [Invisibles, les travailleurs du clic](https://www.casilli.fr/2020/02/12/notre-documentaire-invisibles-les-travailleurs-du-clic-sur-france-televisions-slash-a-partir-du-12-fevr-2020/){:target="_blank"} – une série documentaire d’Antonio A. Casilli, disponible sur son site.

- [La démocratie Internet](https://www.cairn.info/revue-transversalites-2012-3-page-65.htm#no1){:target="_blank"}, un entretien avec Dominique Cardon.

## Droits, statuts, valeur – métamorphoses du travail

### Pour aller au-delà de la construction théorique, vers l’impact concret des évolutions, mutations et autres transformations du droit du travail – depuis le quotidien des travailleurs, jusqu’aux enjeux géopolitiques.

#### [La « collaboration », feuille de vigne du capitalisme de plateforme.](https://www.alterechos.be/la-collaboration-feuille-de-vigne-du-capitalisme-de-plateforme/){:target="_blank"} Les termes «collaboratif» ou «partage» connotent de manière positive des pratiques souvent fort éloignées de cette sémantique.

- [Le salariat n’est pas mort, il bouge encore](https://www.franceculture.fr/emissions/lsd-la-serie-documentaire/le-salariat-nest-pas-mort-il-bouge-encore-14-la-fin-du-salariat){:target="_blank"} Une série documentaire de France Culture en quatre épisodes.

- [« Les plateformes reposent sur des jeunes résignés dans leur rapport au salariat »](https://usbeketrica.com/fr/article/les-plateformes-reposent-sur-des-jeunes-resignes-dans-leur-rapport-au-salariat){:target="_blank"} un entretien de Pablo Maillé pour Usbek et Rica, avec la sociologue Sarah Abdelnour.

#### [Statut d'artiste](https://medor.coop/exclusivite-web/statut-dartiste-le-mythe-12/){:target="_blank"} deux articles de Quentin Noirfalisse pour Médor qui mettent en question ce statut qui, en réalité, est loin d'en être un.

- [« Cartographier la géopolitique des plateformes »](https://legrandcontinent.eu/fr/2020/02/19/geopolitique-des-plateformes/){:target="_blank"} sur Le Grand Continent.

- Alain Supiot, [« Et si l’on refondait le droit du travail ? »](https://www.monde-diplomatique.fr/2017/10/SUPIOT/58009){:target="_blank"}, une tribune parue dans Le monde diplomatique.

- [« Quel droit du travail à l’ère des plateformes numériques ? »](https://www.erudit.org/fr/revues/lsp/2018-n81-lsp04317/1056302ar/){:target="_blank"}, un article de Gwenola Bargain.

- [Deliveroo nous du mal](https://zintv.org/video/deliveroo-nous-du-mal-2/){:target="_blank"}, un débat filmé dans le cadre d’un atelier vidéo ZIN TV en collaboration avec Bruxelles Laïque lors de l’édition 2018 du festival des libertés.

## Que va-t-il advenir du travail ?

### L’analyse, la spéculation, l’exercice est exigeant et libérateur – il engage la responsabilité des auteurs, qui ne sont ni prophètes, ni devins. Sur quels fondements peut-on imaginer ce que sera le travail, quelles formes et quels sens il prendra dans le futur ?

#### [Coopératives de travailleurs : merci, patron !](https://www.alterechos.be/cooperatives-de-travailleurs-merci-patron/){:target="_blank"} L'émergence du modèle coopératif et le constat qu’un grand nombre d’entreprises wallonnes seront exposées à la reprise dans les prochaines années. Additionnez le tout et vous obtenez, au sein du gouvernement wallon, des dispositions visant à favoriser la mise sur pied de «coopératives de travailleurs», inspirées des SCOP à la française.

#### Tout le monde y perd à se faire livrer de la bouffe à domicile. [L'inverview de JB (Shift) par Matéo Vigné pour VICE](https://www.vice.com/fr/article/z3nbme/deliveroo-shift-livraison-domicile-neoliberalisme){:target="_blank"} 

- [L’avenir du travail](https://www.franceculture.fr/emissions/matieres-a-penser-avec-antoine-garapon/matieres-a-penser-droit-et-justice-du-jeudi-21-juin-2018){:target="_blank"}, entretien avec Alain Supiot dans l’émission Matières à penser sur France Culture.

#### [Deliveroo : fin de la course ?](https://www.alterechos.be/deliveroo-fin-de-la-course/){:target="_blank"} Vingt-cinq ans qu’Alter Échos explore les politiques sociales en Wallonie et à Bruxelles. Chaque mois, notre revue a décidé de replonger dans ses archives. Cette fois-ci, l’actualité nous ramène dans la folle bataille des coursiers de Deliveroo et la fragilisation du salariat par les grandes plateformes.

#### La situation des coursiers reflète celle, plus générale, de tous les travailleurs précaires. Plus que jamais, un statut de travail correct et protégé constitue une sorte de luxe, dont le socle diminue d’année en année.[Shift : quel statut pour les coursiers ? Par la Revue Politique](https://www.revuepolitique.be/shift-quel-statut-pour-les-coursiers/){:target="_blank"} 

- [Télétravail. Vous avez dit progrès social ?](https://travailleraufutur.fr/teletravail-vous-avez-dit-progres-social/){:target="_blank"} - chronique philo-critique de Fanny Lederlin dans la revue en ligne Travailler au futur (T.A.F.)

- [Vers la société libérée](https://www.youtube.com/watch?v=zKZO-38HQ04) - Interview d'André Gorz par Marie-France Azar pour l'émission « A voix nue » sur France Culture en 1991. A la même époque, André Gorz proposait également cette tribune : [Pourquoi la société salariale a besoin de nouveaux valets](https://www.monde-diplomatique.fr/1990/06/GORZ/42679){:target="_blank"}.

#### [Le Journal de Culture et Démocratie n°48](https://www.cultureetdemocratie.be/productions/view/travail){:target="_blank"} explore à la question du travail et propose « des repères théoriques et expérientiels, des perspectives et du recul ».

- Une création sonore à partir d'un texte, ["1848"](https://www.radiopanik.org/emissions/les-promesses-de-l-aube/la-jacqueline-lylac-culture-democratie/){:target="_blank"} écrit par un livreur Deliveroo réalisée par Radio Panik.

- [« Vers une bullshit economy »](https://www.liberation.fr/debats/2020/05/27/vers-une-bullshit-economy_1789579/){:target="_blank"}, la tribune de David Graeber dans Libération

- [Entretien avec Alain Damasio](https://lavolte.net/interview-dalain-damasio/){:target="_blank"} pour La Volte, sa maison d’édition.

- [Repenser le futur du travail](https://www.wake-up.io/conversations-e26-dominique-meda/){:target="_blank"}, un entretien avec Dominique Meda.

## État des luttes

### L’espace de travail, et les organisations qui l’occupent, continuent d’être le lieu de luttes, de tensions – qui appellent à la prise de position politique, autant qu’à l’analyse.

#### [« Penser contre le travail » plutôt que « repenser le travail  »](https://www.alterechos.be/penser-contre-le-travail-plutot-que-penser-le-travail/){:target="_blank"} «Que fais-tu dans la vie? Je travaille pour… Je suis consultant chez…» Le travail tient, parfois malgré nous, une position centrale dans nos vies et dans la définition de nos identités. Ceux qui en ont un ne le perdraient pour rien au monde et ceux qui n’en ont pas culpabilisent de ne pas faire partie du marché de l’emploi. Cette situation est insoutenable selon les auteurs de l’ouvrage collectif «Le travail, et après?» (Écosociété, 2017).

#### [« Uber m’a tuer »](https://www.youtube.com/watch?v=1CrX7bbyZ8c){:target="_blank"} - une conférence de Sophie Bernard et Sarah Abdelnour pour Point Culture.

#### [Boulots de merde !](https://www.alterechos.be/boulots-de-merde/){:target="_blank"} Ils sont cireurs de chaussures, infirmiers, distributeurs de prospectus, ou gestionnaire de patrimoine… Leur point commun: ils font des boulots de merde. Julien Brygo et Olivier Cyran, journalistes, qui, comme ils le confient d’emblée «font donc partie du club», ont décidé d’aller à la rencontre de ces métiers, y compris les plus utiles socialement, rongés par la précarisation.

- [« Germinal au royaume des plates-formes numériques ? »](https://www.youtube.com/watch?v=_ztnBHZiASE){:target="_blank"} - Entretien de Jérôme Pimot avec Rachida El-Azzouzi.

- Evgeny Morozov, [« Résister à l’ubérisation du monde »](https://www.monde-diplomatique.fr/2015/09/MOROZOV/53676){:target="_blank"} paru dans Le monde diplomatique

- [« Travail: démocratiser, démarchandiser et dépolluer ».](https://plus.lesoir.be/299599/article/2020-05-16/travail-democratiser-demarchandiser-et-depolluer){:target="_blank"} Cette tribune est parue dans Le Soir, avec parmi les signataires : Isabelle Ferreras, Auriane Lamin, Dominique Méda…

- ["On va globaliser la lutte sociale !" : Quand les livreurs Deliveroo organisent la résistance à l'échelle européenne »](https://www.lesinrocks.com/2018/12/05/actualite/societe/va-globaliser-la-lutte-sociale-quand-les-livreurs-deliveroo-organisent-la-resistance-lechelle-europeenne/){:target="_blank"} paru dans les Inrockuptibles au moment de la création de la F.T.C. à Bruxelles.

- [Travail à la demande, un futur qui se casse la gueule](https://www.joc.be/travail-a-la-demande-un-futur-qui-se-casse-la-gueule/){:target="_blank"}, une enquête des J.O.C. (Jeunes Organisés et Combatifs).

- Pour suivre, en détails, l’évolution des nombreuses actions en justice intentées contre Uber aux U.S.A. : [www.uberlawsuit.com](http://www.uberlawsuit.com/){:target="_blank"} (Site en anglais)

- En Californie, [le syndicat Calaborfed](https://calaborfed.org/stop-uber-and-lyft-from-buying-their-own-law-to-cheat-workers-no-on-prop-22/){:target="_blank"} a mené la lutte contre la « Proposition 22 », massivement soutenue et financée par les plateformes Uber et Lyft.

- À Milan, une procédure exceptionnelle a récemment été mise en place contre Uber Eats. Le grand continent offre [un suivi détaillé](https://legrandcontinent.eu/fr/2020/06/07/procedure-dexception-contre-uber-eats/){:target="_blank"} de l’affaire.

## Le travail, à quel prix ?

### Comment mesurer, comprendre de l’extérieur, l’impact du travail sur les corps et les esprits ?

#### [Temps de travail : remettre les pendules à l’heure.](https://www.alterechos.be/temps-de-travail-remettre-les-pendules-a-lheure/){:target="_blank"} Travaille-t-on de plus en plus? En principe, non. La durée légale du temps de travail n’a pas bougé. En pratique, oui. Ce qui a changé, c’est un management qui a fait de la disponibilité du travailleur un impératif économique et presque une règle morale. Et si on recommençait à compter ses heures?

- [« L’ubérisation met en danger la vie des travailleurs »](https://usbeketrica.com/fr/article/l-uberisation-met-en-danger-la-vie-des-travailleurs){:target="_blank"}, une tribune de Fabien Benoit pour Usbek et Rica.

#### [« On confond douleur physique et souffrance morale »](https://medor.coop/nos-series/la-douleur-des-belges/on-confond-douleur-physique-et-souffrance-morale/){:target="_blank"}, Médor explore de façon participative la thématique de la douleur. Ici sont abordées les difficiles conditions de travail qui amène à une souffrance morale.

- Le Financial Times a mis en place un système interactif, sous forme de jeu de rôle, pour nous mettre « dans la peau » d’un conducteur Uber :  [https://ig.ft.com/uber-game/](https://ig.ft.com/uber-game/){:target="_blank"}

- [Un entretien avec Matthieu Lépine](https://www.jefklak.org/allo-penicaud-des-travailleur·euses-meurent-du-covid-19/){:target="_blank"}, recenseur des accidents du travail, réalisé par Jefklak.

#### [Quand Marlon Brando était un prolétaire individualiste.](https://www.alterechos.be/quand-marlon-brando-etait-un-proletaire-individualiste/){:target="_blank"} Sorti en 1954, le film «On the Waterfront» – «Sur les quais» en français – d’Elia Kazan charriait des thèmes qui sont encore d’actualité aujourd’hui. Individualisme, misère, atomisation des travailleurs. En regardant la gueule d’ange de Marlon Brando s’agiter sur l’écran, on se dit que rien n’a vraiment changé.

#### [Figures de la flexibilité : l’impact des nouvelles formes de travail sur la santé](https://gresea.be/Figures-de-la-flexibilite-l-impact-de-nouvelles-formes-de-travail-sur-la-sante){:target="_blank"} – une étude détaillée du GRESEA.

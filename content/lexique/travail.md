Title: Travail
Slug: travail
Type: lexique
Template: lexique
Category: le-travail-qui-vient
Niveau: 4
Date: 2010-12-03 10:20
Video: BY-7BfziCI8
	pUddzTSYpM0
	iEh8HgCdU44
	ZbOuSGLia-4


### LES 99 SECONDES

10 mots et 20 professionnel·le·s de tous bords pour établir un lexique des nouveaux mots du travail numérisé en 99 secondes chrono !

- **Réalisation** : Clara Beaudoux
- **Design**: Welsey Brutus
- **Son** : Thomas Ferrando
- **Interventions** : Laurence Vielle, Gregor Chapelle, Bruno Frère et Fernanda Alt.

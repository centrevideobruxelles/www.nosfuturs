Title: Flexijob
Slug: flexijob
Type: lexique
Template: lexique
Category: le-travail-qui-vient
Niveau: 4
Date: 2010-12-03 10:20
Video: EFmCgsVmsY0
	S9HCgggOGyw:yes
	S6BdqM1DAJk




### LES 99 SECONDES

10 mots et 20 professionnel·le·s de tous bords pour établir un lexique des nouveaux mots du travail numérisé en 99 secondes chrono !

- **Réalisation** : Clara Beaudoux
- **Design** : Welsey Brutus
- **Son** : Thomas Ferrando
- **Intervenant.e.s** : Oumar Diallo, Fabrice Detry et Bruno Frère

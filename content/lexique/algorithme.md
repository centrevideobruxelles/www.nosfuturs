Title: Algorithme
Slug: algorithme
Type: lexique
Template: lexique
Category: le-travail-qui-vient
Niveau: 4
Date: 2010-12-03 10:20
video: d8VEJNZKIgo
	AqssmkwYkGc
	GjRp4fMGXsg
	cdA1a_2vqjo
	f7UUvbLoSOA
	6F3e3yJdwDQ
	TBcu8-V9AN8
	Wcq0SafHmew


### LES 99 SECONDES

10 mots et 20 professionnel·le·s de tous bords pour établir un lexique des nouveaux mots du travail numérisé en 99 secondes chrono !

- **Réalisation** : Clara Beaudoux
- **Design** : Welsey Brutus
- **Son** : Thomas Ferrando
- **Intervenant.e.s** : François Bellenger, Baptistine Smets, Auriane Lamine, Douglas Speulchre, Emma Kraak, Martin Willems, Johan Lepers et Fabrice Detry

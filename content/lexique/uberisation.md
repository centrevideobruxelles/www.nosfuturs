Title: Ubérisation
Slug: uberisation
Type: lexique
Template: lexique
Category: le-travail-qui-vient
Niveau: 4
Date: 2010-12-03 10:20
Video: 6Ybs4zv6PWU
	CkQZFnMi1rc
	NqlCc-2ypxY
	L9nrNh3R6mU
	kiKudOCEog8
	23cS1DsxBlM



### LES 99 SECONDES

10 mots et 20 professionnel·le·s de tous bords pour établir un lexique des nouveaux mots du travail numérisé en 99 secondes chrono !

- **Réalisation** : Clara Beaudoux
- **Design** : Welsey Brutus
- **Son** : Thomas Ferrando
- **Intervenant.e.s** : Laurent Wartel, Fabrice Detry, Bruno Frère, Baptistine Smets, Anne Dufresne et Gregor Chapelle

Title:Économie de plateforme
Slug: economie-de-plateforme
Type: lexique
Template: lexique
Category: le-travail-qui-vient
Niveau: 4
Date: 2010-12-03 10:20
Video: ui2ByLdybM8
	8fPjBmRr3es:yes
	HYPlYURxjoQ:yes
	aOi2f1OAZAc

### LES 99 SECONDES

10 mots et 20 professionnel·le·s de tous bords pour établir un  lexique des nouveaux mots du travail numérisé en 99 secondes chrono !

- **Réalisation** : Clara Beaudoux
- **Design** : Welsey Brutus
- **Son** : Thomas Ferrando
- **Intervenant.e.s** : Martin Willems, Emma Kraak, Bruno Frère et Anne Dufresne

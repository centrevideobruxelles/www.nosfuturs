Title: Déconnexion
Slug: deconnexion
Type: lexique
Template: lexique
Category: le-travail-qui-vient
Niveau: 4
Date: 2010-12-03 10:20
Video: 0vs2vStTeSg
	evq1XFLwF-s:yes
	3jaC4NF7bFo:yes
	DA5pfB4zzrA

### LES 99 SECONDES

10 mots et 20 professionnel·le·s de tous bords pour établir un lexique des nouveaux mots du travail numérisé en 99 secondes chrono !

- **Réalisation** : Clara Beaudoux
- **Design** : Welsey Brutus
- **Son** : Thomas Ferrando
- **Intervenant.e.s** : Douglas Sepulchre, Giseline Rondeau, Baptistine Smets et Anne Dufresne

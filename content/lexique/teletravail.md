Title: Télétravail
Slug: teletravail
Type: lexique
Template: lexique
Category: le-travail-qui-vient
Niveau: 4
Date: 2010-12-03 10:20
Video: nyid7JMPzr8
	Qw4oWNWmxWo
	jHDS29eVkIQ
	HrgYDoKPFD0
	GGKIL1GX3BI
	ZZ0Ub9KBC7I



### LES 99 SECONDES

10 mots et 20 professionnel·le·s de tous bords pour établir un lexique des nouveaux mots du travail numérisé en 99 secondes chrono !

- **Réalisation** : Clara Beaudoux
- **Design** : Welsey Brutus
- **Son** : Thomas Ferrando
- **Intervenant.e.s** : Sandrino Graceffa, Giseline Rondeau, Bruno Frère, Auriane Lamine et Gregor Chapelle

 

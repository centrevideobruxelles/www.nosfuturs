Title:  edito-nf
slug:  edito
Template: edito_nf
Date: 2010-12-03 10:20
Modified: 2010-12-05 19:30
Summary: description home
Status: published
 
## Bienvenue sur nosfuturs.net !

### nosfuturs.quoi ?
nosfuturs.net est un site de créations documentaires et transmédia pour mieux comprendre les mondes qui viennent.

Créé et produit par le [CVB - Centre Vidéo de Bruxelles](https://cvb.be/fr){:target="_blank"} nosfuturs.net est évolutif et volontairement *low-tech* : un site peu énergivore dont la sobriété technique et graphique invite à renouveler la perception et l'usage des interfaces numériques qui nous entourent.

Centré sur une première thématique transversale intitulée *Le travail qui vient*, nosfuturs.net fonctionne comme un archipel de contenus déployés autour d’un documentaire d’auteure (le film SHIFT) et propose un podcast, des moyens et courts métrages, des capsules vidéos et une sélection de ressources sur les enjeux futurs du travail.


### nosfuturs.pourquoi ?
La crise que nous traversons exacerbe le besoin de mobiliser d’autres imaginaires, d’inventer des récits dépeignant une autre manière de faire société. Des récits qui puissent parler à chacun de sa réalité tout en permettant de nous projeter, les pieds bien ancrés dans le sol, dans un futur plus engageant. C’est en documentant et en confrontant plusieurs « versions » de l’avenir que l’on pourra en écrire d’autres scenarios. Non pas s’attacher à dépeindre un « monde meilleur » qui ne viendra jamais, mais bien se retrousser les manches pour mieux appréhender, dans une multitude de visions et de formats, nos possibles futurs.


var BOXCLICK, BOXTEXT
var P_PODCAST
var B_CLOSE
var B_INFO
var _LIST_
var FLEXMAIN 
var INTRO 

function liens_activation(){

	var activation = function(id){
		var lien = document.getElementById(id)
		lien.classList.add('lien-actif')
	}

	var deactivation = function(id){
		var lien = document.getElementById(id)
		lien.classList.remove('lien-actif')
	}

	var ev_click = function(el){
		var summary = el.getElementsByTagName('summary')
		var details = el.getElementsByTagName('details')
		summary.addEventListener('click', function(){
			var id_liens = el.getAttribute('data-liens').split(' ')
			if (details[0].open !== true){
				id_liens.forEach(id => activation(id));
			}else {
				id_liens.forEach(id => deactivation(id));
			}
		})
	}
	BOXCLICK.forEach(el => ev_click(el));
}

function xh(url, playerId) {
	xhr = new XMLHttpRequest()
	xhr.open("GET", url, false)
	xhr.onreadystatechange = function() {
		if (xhr.readyState === 4 && xhr.status == "200") {
			var c = xhr.responseText
			var P_ = document.getElementById('box-'+playerId)
			P_.querySelector('.content').innerHTML = c 
			if(playerId == 'bottom') {
				player()
				btn_info(P_)
			}
			P_.classList.add('open')	
			FLEXMAIN.classList.add('open_'+playerId)	
		}
	}
	xhr.send("")	
}

function click_media() {
	BOXCLICK.forEach(el => load_media(el));
}

function load_media(el) {
	el.addEventListener('click', function(){
		var url = this.getAttribute('data-url')
		var playerId = this.getAttribute('data-player')
		xh(url, playerId)
	})
}

function close_media() {
	B_CLOSE.forEach(function(item, i){
		item.addEventListener('click', function(){
			var par = this.closest('.box-reader')
			par.classList.remove('open')
		 	pos = par.id.split('-')[1]
			FLEXMAIN.classList.remove('open_'+pos)
		}) 
	})
}

function btn_info(zone){
	var btn = zone.getElementsByClassName('btn_info')
	btn[0].addEventListener('click', function(){
		var content = zone.getElementsByClassName('info')
		console.log(content)
		FLEXMAIN.classList.toggle('open_bottom_info')
	})
}

window.addEventListener('DOMContentLoaded', function(){
	BOXCLICK = document.querySelectorAll('.box-click')
	BOXTEXT = document.querySelectorAll('.box-Lexique-Text')
	P_PODCAST = document.getElementById('box-playerPodcast')
	B_CLOSE = document.querySelectorAll('.close')
	FLEXMAIN = document.querySelector('.flexdetout')
	INTRO = document.querySelector('.introduction')
	close_media()
	click_media()
	placement()
	if (location.hash == '#home') {
		INTRO.removeAttribute("open")
	}
})

Title: Curriculum Futuræ
slug: curriculum-futuræ
Category: le-travail-qui-vient
Type: podcast
Template: podcast
Niveau: 2
Date: 2021-05-01 10:20
Video: <iframe width="100%" height="330" scrolling="no" frameborder="no" src="https://audio.liquorstane.ynh.fr/front/embed.html?&amp;type=album&amp;id=18"></iframe>


## Un podcast en 4 épisodes de Sébastien Dicenaire

### Un père de famille anticipe (beaucoup) la recherche de travail de ses enfants

À l'heure de l'ubérisation, de la robotisation des tâches et des algorithmes auto-apprenants, un père trentenaire un brin flippé s'inquiète de l'avenir professionnel de ses jumeaux de 5 ans... À la rencontre de spécialistes de tous bord comme de travailleur·euse·s actuel·le·s, il mène l'enquête sur les **scénarios futurs** du travail.

*Sébastien Dicenaire est écrivain, performeur et créateur radiophonique ...*

![](img/01.jpg)


EP1 : 
https://cf-media.sndcdn.com/ZWEqkPGeeqvE.128.mp3?Policy=eyJTdGF0ZW1lbnQiOlt7IlJlc291cmNlIjoiKjovL2NmLW1lZGlhLnNuZGNkbi5jb20vWldFcWtQR2VlcXZFLjEyOC5tcDMiLCJDb25kaXRpb24iOnsiRGF0ZUxlc3NUaGFuIjp7IkFXUzpFcG9jaFRpbWUiOjE2MTcxMDAxNjh9fX1dfQ__&Signature=C-51iB5jRpvijTn9Jl2LRuYzqLUsrVaF3qpLmSa879OrQ8DDBwnm0~KHszp~jKbD5D55qYiw9EF8OHiJGHZdXE4CnizdA3SiabYLWVKU6-orFC9KeGOVV6NKqLxXdEUQortTjNSdTrRboHIhSjxTiSNNeGeieBo5T1jm2fD~aqiZHC7XXYXcgeCbctLDhH~CRZFF9UUeKnChJogIFbvwOatr5nB5AigryCbc4t7qcJj73e4Gl6v8i2ZuRHB1nH7~3WuZvTGk7Y0EhCjxIWftodxWXfWZ0kBbCp9tvEOjGyv71PxVc0h4QZS7ZLm3ofJ1hQTdbwnB6ZNiQ1aB9k7Djg__&Key-Pair-Id=APKAI6TU7MMXM5DG6EPQ

Un jeune père de famille fait un cauchemard, c'est deux enfants doivent trouver un job, ils ont 5 ans. 

EP2 :
https://cf-media.sndcdn.com/E8vCWP5oQm8g.128.mp3?Policy=eyJTdGF0ZW1lbnQiOlt7IlJlc291cmNlIjoiKjovL2NmLW1lZGlhLnNuZGNkbi5jb20vRTh2Q1dQNW9RbThnLjEyOC5tcDMiLCJDb25kaXRpb24iOnsiRGF0ZUxlc3NUaGFuIjp7IkFXUzpFcG9jaFRpbWUiOjE2MTcxMDAwNTJ9fX1dfQ__&Signature=JVOSLN05jUJzU7~xHnHkCKa1978cdvfqHv7N6N98YsnUA5cPoUMrbacAbumxRFxCE6gPQ85ZIPZYMz5jAhRpXnL0RZKomqAdlcGSR3421AKb8YzbV2Fy24MEFq3AZCkS8CkjAVlSwKQwD2g9SSWiBCUaAjCoxtj1L7QNqO-iuQNqiW2lcdZBXNLObXh4PzU-OIotmFsZ0okdXrb36-iBUSfwaKF5mKHrxsFNak9YEjb2wSRLxmcRaec0uHjJbYDE2kpNmVmmHMAEFORwjBn~DUyfjqywKTQgks0gtKiNXwFJ-Ekk8zHSRp4D8jZqjv9DBAysuHaxfAt~2SP8mtgG6Q__&Key-Pair-Id=APKAI6TU7MMXM5DG6EPQ

L'enquête continue, lex experts et le reste de la famille s'en mèlent.

Title: Alter Echo - Courant Mai
slug: alter_echo_courant_mai
Category: le-travail-qui-vient
Type: texte 
Niveau: 3
Date: 2010-12-03 10:20

<p> Texte random : Ce qui est intéressant est l’appel à la prise en compte du bien-être des salariés. Le texte en fait un enjeu managérial. Par le biais de l’accord sur le télétravail, les partenaires sociaux demandent de prendre en considération les particularités de l’environnement de chacun. C’est la première fois qu’on le formalise dans un texte. Ainsi, les employeurs sont appelés à organiser des formations, car le télétravail nécessite une aptitude plus forte à utiliser les outils numériques.

L’article 5 souligne que le télétravail peut isoler le salarié, il est essentiel d’en parler. Le texte est l’occasion de mettre à plat des pratiques sociales. Dans le cas du télétravail, l’accord explique que le salarié ne doit pas subir une dégradation des conditions de travail et doit être disponible pour l’entreprise, cela fonctionne dans les deux sens.

Un autre point important est celui du remboursement des frais engagés par un salarié dans le cadre de l’exécution de son contrat de travail. A ce titre, il appartient ainsi à l’entreprise de prendre en charge les dépenses qui sont engagées par le salarié pour les besoins de son activité professionnelle et dans l’intérêt de l’entreprise, après validation de l’employeur. On ne se pose pas la question quand on va travailler en entreprise.
